
import net.sf.javailp.Linear;
import net.sf.javailp.OptType;
import net.sf.javailp.Problem;
import net.sf.javailp.Result;
import net.sf.javailp.Solver;
import net.sf.javailp.SolverFactory;
import net.sf.javailp.SolverFactoryLpSolve;
import lpsolve.*;


public class Rep_Alg06_PrivateLP 
{

	 
	 public static void Algorithm()
	 {
		 repTools.reset();
		 repTools.tablesInit();
		 repTools.replicaSetInit();
		 //repTools.replicaSetGenerator(repTools.PrivateOptimizer(repTools.realDistance, repTools.getProblemSize()), "Real", repTools.getProblemSize());
		 repTools.replicaSetGenerator2(repTools.PrivateOptimizer(repTools.nameidsDistance, repTools.getNameSpace()), "Local", repTools.getNameSpace());
		 repTools.replicaAssignmentSetGenerator(repTools.getProblemSize());
		 //realWordTransform();
		 double localDelay = repTools.privateAverageDelay(repTools.realWorldReplicaAssignment);
		 //int realDelay  = repTools.privateTotalDelay(repTools.realReplicaAssignment);
 
//		 double ratio = (double)localDelay/realDelay;
		 System.out.println("Average Delay " + localDelay);
		 repTools.setRatioDataSet(system.simIndex - 1, localDelay);
		 
		 
       if(system.simIndex == system.simRun && system.repEvaluation)
		{
    	   repTools.evaluation(" Algorithm 06 PrivateLP ");  
		}
       if(system.loadEvaluation)
       {
    	   repEvaluation.loadEvaluation();	
       }
         
	 }
}
