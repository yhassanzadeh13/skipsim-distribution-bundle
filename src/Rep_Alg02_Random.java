
import java.util.Random;

import net.sf.javailp.Linear;
import net.sf.javailp.OptType;
import net.sf.javailp.Problem;
import net.sf.javailp.Result;
import net.sf.javailp.Solver;
import net.sf.javailp.SolverFactory;
import net.sf.javailp.SolverFactoryLpSolve;
import lpsolve.*;


public class Rep_Alg02_Random 
{
     
     public static void randomReplicaGenerator()
     {
    	 Random random = new Random();
    	 for(int i = 0; i < repTools.getMNR() ; i++)
    	 {
    		 int index = random.nextInt(system.size);
    		 while(repTools.realWorldReplicaSet[index])
    			 index = random.nextInt(system.size);
    		 
    		 repTools.realWorldReplicaSet[index] = true;
    	 }
    	 
    	 for(int i = 0 ; i < system.size ; i++)
    	 {
    		 int closestReplica = 0;
    		 int closestReplicaDistance = Integer.MAX_VALUE;
    		 for(int j = 0 ; j < system.size ; j++)
    		 {
    			 if(repTools.realWorldReplicaSet[i])
    				 if(nodes.nodeSet[i].Coordination.distance(nodes.nodeSet[j].Coordination) < closestReplicaDistance)
    				 {
    					 closestReplica = j;
    					 closestReplicaDistance = (int) nodes.nodeSet[i].Coordination.distance(nodes.nodeSet[j].Coordination);
    				 }
    		 }
    		 
    		repTools.realWorldReplicaAssignment[closestReplica][i] = true;
    	 }
    		 
     }
	 
	 public static void Algorithm()
	 {
		   repTools.reset();
		   double ratio = 0;
		   for(int i = 0; i < repTools.getExperimentNumber() ; i++)
		   {
			 repTools.tablesInit();
			 repTools.replicaSetInit();
			 repTools.replicaSetGenerator(repTools.PublicOptimizer(repTools.realDistance, system.size), "Real", system.size);
			 randomReplicaGenerator();
			 int localDelay = repTools.publicTotalDelay(repTools.realWorldReplicaAssignment);
			 int realDelay  = repTools.publicTotalDelay(repTools.realReplicaAssignment);
			 
			 ratio = ratio + (double)localDelay/realDelay;
		   }
			 ratio = ratio / repTools.getExperimentNumber();
		     System.out.println("Ratio " + ratio);
			 repTools.setRatioDataSet(system.simIndex - 1, ratio);
		 
		 if(system.simIndex == system.simRun)
		 {
				repTools.evaluation(" Algorithm 02 Random ");
		 }
         
	 }
}