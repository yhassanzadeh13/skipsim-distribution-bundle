import java.awt.Point;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class fileInteractions
{
  public static void readConfigFile()
  {
		 try
		 { 
			 BufferedReader  in	 = new BufferedReader(new FileReader("config.txt"));
			 String command = in.readLine();
			 while(!command.equals("End of configuration")) 
			 {
				 if(command.startsWith("//"))
				 {
					 command = in.readLine();
					 continue;
				 }
				 else if(command.contains("nodes.number"))
				 {
					system.size = extractValueFromString(command);
				 }
				 
				 else if(command.contains("nodes.nameIDSize"))
				 {
					system.nameIDsize = extractValueFromString(command);
				 }
				 
				 else if(command.contains("nodes.generation"))
				 {
					 system.nodeGeneration = extractStringParameterFromString(command);
				 }
				 
				 else if(command.contains("landmarks.number"))
				 {
					system.landmarks = extractValueFromString(command);
				 }
				 
				 else if(command.contains("landmarks.prefix"))
				 {
					system.prefix = extractValueFromString(command);
				 }
				 
				 else if(command.contains("domain"))
				 {
					system.domainSize = extractValueFromString(command);
				 }
				 
				 else if(command.contains("iterations"))
				 {
					system.simRun = extractValueFromString(command);
				 }
				 
				 else if(command.contains("nodes.numericalIDAssignment"))
				 {
					 system.numericalIDAssignment = extractStringParameterFromString(command);
				 }
				 
				 else if(command.contains("nodes.nameIDAssignment"))
				 {
					 system.nameIDAssignment = extractStringParameterFromString(command);
				 }
				 
				 else if(command.contains("replication.type"))
				 {
					 system.replicationType = extractStringParameterFromString(command);
				 }
				 
				 else if(command.contains("replication.algorithm"))
				 {
					 system.replicationAlg = extractStringParameterFromString(command);
				 }
				 
				 else if(command.contains("replication.MNR"))
				 {
					 system.MNR = extractValueFromString(command);
				 }
				 
				 else if(command.contains("replication.NOR"))
				 {
					 system.NOR = extractValueFromString(command);
				 }
				 
				 else if(command.contains("search.nameID"))
				 {
					 system.searchByNameID = extractValueFromString(command);
				 }
				 
				 else if(command.contains("search.numericalID"))
				 {
					 system.searchByNumericalID = extractValueFromString(command);
				 }
				 
				 else if(command.contains("evaluation.idEvaluation"))
				 {
					 system.idEvaluation = true;
				 }
				 
				 else if(command.contains("evaluation.repEvaluation"))
				 {
					 system.repEvaluation = true;
				 }
				 
				 
				 else if(command.contains("evaluation.loadEvaluation"))
				 {
					 system.loadEvaluation = true;
				 }
				 
				 command = in.readLine();
				 		         
			 }

     	
			 in.close();
		 } 
		 catch (FileNotFoundException e) 
		 {
			// TODO Auto-generated catch block
			e.printStackTrace();
		 }
		 catch (NumberFormatException e)
		 {
			// TODO Auto-generated catch block
			e.printStackTrace();
		 } 
		 catch (IOException e) 
		 {
			// TODO Auto-generated catch block
			e.printStackTrace();
		 }
  }
  
  private static int extractValueFromString(String str)
  {
	  int startIndex = str.indexOf('$') + 1;
	  String value   = new String();
	  for(int i = startIndex; i < str.length(); i++)
	  {
		  if(str.charAt(i) == '$')
			  break;
		  else
			  value = value + str.charAt(i);
			  
	  }
	  
	  return Integer.parseInt(value);
  }
  
  private static String extractStringParameterFromString(String str)
  {
	  int startIndex = str.indexOf('$') + 1;
      String stringParameter = new String();
	  for(int i = startIndex; i < str.length(); i++)
	  {
		  if(str.charAt(i) == '$')
			  break;
		  else
			  stringParameter = stringParameter + str.charAt(i);
			  
	  }
	  
	  return stringParameter;
  }
  
  public static void showTheConfig()
  {
	  System.out.println("**************************Welcome to SkipSim Ver 1.0*****************");
	  System.out.println("A simulation has been configured with ");
	  System.out.println("Number of nodes:  " + system.size);
	  System.out.println("Number of landmarks: " + system.landmarks);
	  System.out.println("Name id size: " + system.nameIDsize);
	  System.out.println("Landmarks' prefix size: " + system.prefix);
	  System.out.println("Number of topologies: " + system.simRun);
	  System.out.println("Size of domain: " + system.domainSize);
	  if(system.nodeGeneration.equals("landmark"))
		  System.out.println("Nodes are distributed based on the nodes manifestation probability");
	  else
		  System.out.println("Nodes are distributed uniformely at random");
	  
	  System.out.println("Numerical id assignmnet of nodes is " + system.numericalIDAssignment);
	  System.out.println("Name id assignment of nodes is " + system.nameIDAssignment);
	  
	  if(!system.replicationAlg.equals("None"))
	  {
		  System.out.println("Replication algorithm is " + system.replicationAlg);
		  System.out.println("Number of replicas " + system.MNR);
		  if(system.replicationType.equals("public"))
			  System.out.println("Replication type is public");
		  else
		  {
			  System.out.println("Replication type is private");
			  System.out.println("A set of " + system.NOR + " nodes are chosen uniformly at random as data requester nodes");
		  }
		  if(system.repEvaluation)
			  System.out.println("Replication algorithm is evaluated based on access delay");
		  if(system.loadEvaluation)
			  System.out.println("Replication algorithm is evaluated based on load distribution among replicas");
	  }
		  
	  	
	  if(system.idEvaluation)
		  System.out.println("Name ids are evaluated based on their locality awareness");
	  if(system.searchByNameID != 0)
		  System.out.println(system.searchByNameID + " random search by name id will be initiated");
	  if(system.searchByNumericalID != 0)
		  System.out.println(system.searchByNumericalID + " random search by numerical id will be initiated");
	  
	  System.out.println("**************************End of Configuration*****************");

	  

		  
  }
    
}