
import net.sf.javailp.Linear;
import net.sf.javailp.OptType;
import net.sf.javailp.Problem;
import net.sf.javailp.Result;
import net.sf.javailp.Solver;
import net.sf.javailp.SolverFactory;
import net.sf.javailp.SolverFactoryLpSolve;
import lpsolve.*;


public class Rep_Alg01_LP 
{	 
	 public static void Algorithm()
	 {
		 repTools.tablesInit();
		 repTools.replicaSetInit();

		 //repTools.replicaSetGenerator2(repTools.PublicOptimizer(repTools.nameidsDistance, repTools.getNameSpace()), "Local", repTools.getNameSpace());
		 repTools.replicaSetGenerator(repTools.PublicOptimizer(repTools.realDistance, repTools.getProblemSize()), "Real", repTools.getProblemSize());
		 //realWordTransform();
		 repTools.replicaAssignmentSetGenerator(repTools.getProblemSize());
		 int localDelay = repTools.publicTotalDelay(repTools.realWorldReplicaAssignment);
		 int realDelay  = repTools.publicTotalDelay(repTools.realReplicaAssignment);
 
		 double ratio = (double)localDelay/realDelay;
		 System.out.println("Ratio " + ratio);
		 repTools.setRatioDataSet(system.simIndex - 1, ratio);
		 
       if(system.simIndex == system.simRun && system.repEvaluation)
		{
    	   repTools.evaluation(" Algorithm 01 LP ");
		}
       if(system.loadEvaluation)
       {
    	   repEvaluation.loadEvaluation();	
       }
         
	 }
}
