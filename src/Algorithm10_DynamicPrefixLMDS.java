import mdsj.ClassicalScaling;
import mdsj.MDSJ;
public class Algorithm10_DynamicPrefixLMDS 
{
	
	  
	  /**
	   * The adjacency matrix adj[i][j] is the distance of the landmark i to node j	 
	   **/
	  public static double[][] adj = new double[system.size][system.size];	
	  
	  /**
	   * Init determines that whether the algorithm initialized yet or not, it only be used in the algorithm function
	   */
	  public static boolean    init= true;
	  
	  /**
	   * The result of LMDS operation would save at this matrix
	   */
	  public static double[]  LMDS = new double[system.size];
	  
	  /**
	   * namespace is an array which saves the all possible name ids and namespaceindex will save the next available space for saving a name id
	   * this index only is used in the nameIDGenerator
	   */
	  public static String[] nameSpace = new String[system.size];
	  private static int nameSpaceIndex = 0;
	  
	  /**
	   *The LMDS function will perform the LMDS algorithm using the MDSJ library 
	   */
	  public static void LMDS()
	  {
			double[][] output = new double[2][system.size]; 
			ClassicalScaling.lmds(adj, output);
			for(int i = 0 ; i < LMDS.length ; i++) //Converting from two dimension to one dimension
			{  
			    LMDS[i] = Math.sqrt(Math.pow(output[0][i], 2) + Math.pow(output[1][i], 2));
			    System.out.println("LMDS " + i + " " +LMDS[i]);

			}
	  }
	  
	  public static void reset()
	  {
		  adj  = new double[system.size][system.size]; 
		  LMDS = new double[system.size];
		  nameSpace = new String[system.size];
		  nameSpaceIndex = 0;
	  }
	  
	  /**
	   * This function will generates all the name ids with the n bit argument (Permutations)
	   * @param n number of the bits of the name id
	   */
	  public static void nameIDGenerator(int n)
	  {
		        String B;
		    	for(int i = 0; i < Math.pow(2,n); i++)
		        {
		            B = "";
		            int temp = i;
		            for (int j = 0; j < n; j++)
		            {
		                if (temp%2 == 1)
		                    B = '1'+B;
		                else
		                    B = '0'+B;
		                    temp = temp/2;
		            }
		            //System.out.println(B);
		            addToNamespace(B);
		         }
		     
	  }
	  
	  public static void addToNamespace(String B)
	  {
		  nameSpace[nameSpaceIndex] = B;
		  nameSpaceIndex++;
	  }
	  
	  /**
	    * This function will make the adjacent matrix the element i,j is corresponding to the distance of the node
	    * i to the landmark j this function.
	   **/
	  public static void makingAdj()
	  {
	
		  for(int i = 0 ; i < system.size ; i++)
			  for(int j = 0 ; j < system.size ; j++)
			  {
				  adj[i][j] = nodes.nodeSet[i].Coordination.distance(nodes.nodeSet[j].Coordination);
			  }
		  
	  }
	  
	  /**
	   * Finding the min item in the LMDS array
	   * Before returning the min value's index
	   * it will eliminite the min value from the 
	   * array 
	   * @return min value's index in LMDS array
	   */
	  
	  public static int findMin()
	  {
		  int index = 0;
		  double min = Double.MAX_VALUE;
		  for(int i = 0 ; i < LMDS.length ; i++)
		  {
			  if(min > LMDS[i])
			  {
				  min = LMDS[i];
				  index = i;
			  }
		  }
		  LMDS[index] = Double.MAX_VALUE;
		  return index;
	  }	
	  
	  /**
	   * Generates the name ids based on their LMDS values
	   * in the ascending order
	   * 
	   */
	  public static void nameIDGeneration()
	  { 
		  Landmark.upDateDynamicPrefix();
		  
		  for(int i = 0 ; i < nodes.nodeSet.length ; i++)
		  {
			  
			  int nodeIndex = findMin();
			  int prefix = ClosestLandmark(nodes.nodeSet[i]);
			  nodes.nodeSet[i].nameID = Landmark.dynamicPrefix[prefix] + nameSpace[i];
			  System.out.println("The name ID of the node " + i + "  is " + nodes.nodeSet[i].nameID);
		  }
	  }
	  
	 public static int ClosestLandmark(node n)
	 {
		 double min = Double.MAX_VALUE;
		 int index = 0;
		 for(int i = 0 ; i < system.landmarks ; i++)
		 {
			 if(n.Coordination.distance(Landmark.Set[i]) < min)
			 {
				min = n.Coordination.distance(Landmark.Set[i]);
				index = i;
			 }
		 }
		 
		 return index;
	 }
	  
	  public static void printADJ()
	  {
		  for(int i = 0 ; i < system.landmarks ; i++)
		  {
			  for(int j = 0 ; j < system.size ; j++)
			  {
				  System.out.print((int)adj[i][j] + "  ");
			  }
			  System.out.println("    ");
		  }
	  }
	  public static void Algorithm()
	  {
	
			 makingAdj();
	         LMDS();
	         //printADJ();
	         nameIDGenerator(system.nameIDsize);
	         nameIDGeneration();
	         
	         for(int i = 1 ; i < system.size ; i++)
	         {
	        	 //System.out.println(nodes.nodeSet[i].nameID);
	        	 SkipGraphOperations.Insert(i);
	         }
	         
	  }
	  
	  
	  
	  
	



}
