import java.awt.Point;


/**
 * 
 * @author Yahya Hassanzadeh
 * @version 1.0
 * 
 * This class tries to find the closest node in the skip graph to an external node 
 * that is not a member of the skip graph
 * The main function of this class is the Algorithm(node n) which receives a node "n" and 
 * will return the index of the closest node to this node (node n) 
 *
 */
class ClosestNode 
{
  /**
   * The distance of the closest node to the node n 
   */
  static double minDistance = Double.MAX_VALUE;
  
  /**
   * The index of the closest node that will finally be returned by the Algorithm function 
   */
  static int    minIndex    = -1;
  
  /**
   * The boolean array that holds the visited node
   * each node has a corresponding boolean value true or flase
   * True = Visisted
   * False = not visisted yet
   */
   static boolean[] nodeVisit = new boolean[system.size];
  
   /**
    * How far the startIndex node (The introducer of the node n)
    * should go far to check for the closest node
    */
   static final int depthLimit = 7;
   
   static double totalTime = 0;
   static double[] timeRecords = new double[depthLimit+1];
   static int faildEstimationNumber = 0;
   
   static double finalAverageFailedEstimation = 0;
   static double finalTotalTime = 0;
   
  /**
   * the reset() function will reset the minDistance and minIndex and also
   * nodeVisit[] to their default values. This function will only be called
   * when the algorithm function starts.
   */
  static void internalReset()
  {
	  totalTime = 0;
	  minDistance = Double.MAX_VALUE;
      minIndex    = -1;
	  for(int i = 0 ; i < system.size ; i++)
		  nodeVisit[i] = true;
	  for(int i = 0 ; i < depthLimit+1 ; i++)
		  timeRecords[i] = 0;
  }
  
  /**
   * to reset the failedEstimateNumber for each topology
   */
  static void externalReset()
  {
	  faildEstimationNumber = 0;
  }
  
  static void FindTheClosest(Point P, int startIndex)
  {
	if(nodes.nodeIndex != 0)
		FindTheClosest(P, startIndex, startIndex, 0);
  }
  
  static void FindTheClosest(Point P, int index, int before , int depth)
  {
	  nodeVisit[index] = false;  
	  if(depth <= depthLimit)
	  {
		  if(timeRecords[depth] <  nodes.nodeSet[index].Coordination.distance(nodes.nodeSet[before].Coordination))
			  timeRecords[depth] = nodes.nodeSet[index].Coordination.distance(nodes.nodeSet[before].Coordination);
	  }
	  if(P.distance(nodes.nodeSet[index].Coordination) < minDistance)
	  {
		  minDistance = P.distance(nodes.nodeSet[index].Coordination);
		  minIndex = index;
	  }
	  
	  depth++;
	  
	  if(depth <= depthLimit)
	  {
		  for(int i = 0 ; i < system.nameIDsize ; i++)
			  for(int j = 0 ; j < 2 ; j++)
			  {
				  if(nodes.nodeSet[index].getLookup(i, j)!= -1)
				  {
					  before = index;
					  FindTheClosest(P, nodes.nodeSet[index].getLookup(i, j), before , depth);
				  }
					  
			  }
	  }
	  else
	  {
		  return;
	  }
		  
  }
  
  public static int Algorithm(node n)
  {
	  internalReset();
	  FindTheClosest(n.Coordination, n.introducer);
	  for(int i = 0 ; i < depthLimit+1 ; i++)
		  totalTime += timeRecords[i];
	  evaluation(n, minIndex);
	  return minIndex;
	  
  }
  
  static int findTheReal(node n)
  {
	 double realMinDistance = Double.MAX_VALUE;
	 int    realMinIndex    = 0;
	 
	 for(int i = 0 ; i < nodes.nodeIndex ; i++)
	 {
		 //if the distance of the node n to the node number i is less than the realMinDistance
		 if(nodes.nodeSet[i].Coordination.distance(n.Coordination) < realMinDistance)
			 {
			   realMinDistance = nodes.nodeSet[i].Coordination.distance(n.Coordination);
			   realMinIndex = i;
			 }
		 
	 }
	 
	 return realMinIndex;
  }
  
  public static void evaluation(node n, int index)
  {
	if(index != findTheReal(n))
		faildEstimationNumber++;
	if(nodes.nodeIndex == system.size-1)
	{
		double accuracy = system.size - faildEstimationNumber;
		accuracy = accuracy / system.size;
		accuracy = accuracy * 100;
		System.out.println("\n\n The accuracy of the simulation is: " 
	    +  accuracy);
		finalAverageFailedEstimation += accuracy;
		finalTotalTime += totalTime;
		
		if(system.simRun == system.simIndex)
		{
			finalAverageFailedEstimation /= system.simRun;
			finalTotalTime /= system.size;
			System.out.println("The overal accuracy of the estimation for " + system.simRun 
					+ " runs is " + finalAverageFailedEstimation + 
					"\nThe total average time to find the closest node " + finalTotalTime);
		}
	}
	
		
  }
  

  
  
  
}
