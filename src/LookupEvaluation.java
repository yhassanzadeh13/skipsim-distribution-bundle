import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;


public class LookupEvaluation 
{


	public static void randomLookUpTest(int iterations)
	{
	     for(int i = 0; i < iterations ; i++)
	     {
	     	  Random random = new Random();
	     	  String nameID = nodes.nodeSet[random.nextInt(system.size-1)].nameID;
	     	  int j = random.nextInt(system.size-1);
	     	  int result = SkipGraphOperations.SearchByNameID(nameID, j);
	     	  if(result < 0)
	     	  {
	     		 System.out.println("Serach for " + nameID + " failed!!");
	     		 continue;
	     	  }
	     	  String nameResult = nodes.nodeSet[result].nameID;

	     	  if(nameResult == nameID)
	     	  {
	     	    System.out.println("Serach for " + nameID + " is " + result + " with name ID  " + nameResult);
	     	  }
	       } 
	 
	}
	
	public static void randomLookUpTest(int searchIndex, int startIndex, String Operation)
	{
 
        if(Operation.contains("nameID"))	 
        {
		      String nameID = nodes.nodeSet[searchIndex].nameID;
		      int result = SkipGraphOperations.SearchByNameID(nameID, startIndex);
	     	  if(result < 0)
	     	  {
	     		 System.out.println("Name ID Search for " + nameID + " failed!!");
	     	  }
	     	  String nameResult = nodes.nodeSet[result].nameID;

	     	  if(nameResult == nameID)
	     	  {
	     	    System.out.println("Name ID Search for " + nameID + " is " + result + " with name ID  " + nameResult);
	     	  }
        }
        else
        {
		      int numID = nodes.nodeSet[searchIndex].getNumID();
		      int result = SkipGraphOperations.SearchByNumID(numID, startIndex);
	     	  if(result < 0)
	     	  {
	     		 System.out.println("num ID Search for " + numID + " failed!! from " + nodes.nodeSet[startIndex].getNumID());
	     	  }
	     	  int numResult = nodes.nodeSet[result].getNumID();

	     	  if(numResult == numID)
	     	  {
	     	    System.out.println("num ID Search for " + numID + " is " + result + " with name ID  " + numResult 
	     	    		+ "\nThe search started from " +  startIndex + " with numID "+nodes.nodeSet[startIndex].getNumID() );
	     	  }
        }

	 
	}
	
	

		public static int randomLookUpTest()
		{

     	  Random random = new Random();
     	  int randomIndex = random.nextInt(system.size-1);
     	  String nameID = nodes.nodeSet[randomIndex].nameID;
     	  int startIndex = random.nextInt(system.size-1);
//          int i = 0;
//     	  for(; i < system.size && SkipGraphOperations.commonBits(nameID, nodes.nodeSet[i].nameID)!= 4 ; i++);
//     	  if(i >= system.size || i == randomIndex)
//     		  startIndex = random.nextInt(system.size-1);
//     	  else
//     		  startIndex = i;  
     	  
     	  
     	  
     	  while(SkipGraphOperations.commonBits(nameID, nodes.nodeSet[startIndex].nameID)  < 1)
     		 nameID = nodes.nodeSet[random.nextInt(system.size-1)].nameID;
     	  
     	  
     	  
     	  int result = SkipGraphOperations.SearchByNameID(nameID, startIndex);
     	  if(result < 0)
     	  {
     		 System.out.println("Serach for " + nameID + " failed!!");
     		 return Integer.MAX_VALUE;
     	  }
     	  String nameResult = nodes.nodeSet[result].nameID;

     	  if(nameResult == nameID)
     	  {
     	    System.out.println("Serach for " + nameID + " is " + result + " with name ID  " + nameResult);
     	    return SkipGraphOperations.commonBits(nameID, nodes.nodeSet[startIndex].nameID);
     	  }
     	  
     	 return Integer.MAX_VALUE;
     	  

		}
	
		
    public static double[] Means      = new double[system.simRun];	
	public static void evaluator(int iterations, String Operation)
	{
		if(!FileCheck())
		{
			//System.out.println("FileCheckFalse");
			SeedGenerator(iterations);
		}
		try 
		{
			BufferedReader  in	 = new BufferedReader(new FileReader("LookupRandomSeed.txt"));
	 		double Mean = 0;
			double SD   = 0;
			for(int i = 0 ; i < iterations ; i++)
			{
				nodes.resetTotalTime();
	            int a = Integer.parseInt(in.readLine());
	            int b = Integer.parseInt(in.readLine());
	            randomLookUpTest(a, b, Operation);
	            nodes.addTime(a, b); //including the last message delay
				Mean += nodes.getTotalTime();

			}
			
			in.close();
			Mean /= iterations;
			Means[system.simIndex-1] = Mean;
			System.out.println("Mean " + Mean);

			
			if(system.simRun == system.simIndex)
			{
				Mean = 0;
				for(int i = 0 ; i < system.simRun ; i++)
				      Mean += Means[i];
				
				Mean = Mean/system.simRun;
				
				for(int i = 0 ; i < system.simRun ; i++)
				{
				  SD += Math.pow(Mean - Means[i], 2);	
				  //System.out.println(Means[i]);
				}
				SD = Math.sqrt(SD/system.simRun);
				
				System.out.println("The average rtt time for " + iterations + " random transactions is " + Mean + "With the SD " + SD);	
			}
		}
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
          
		
	}
	
	


	public static void SeedGenerator(int number)
	{
		FileWriter fstream;
		try
		{
			Random random = new Random();
			fstream = new FileWriter("LookupRandomSeed.txt");
			BufferedWriter out = new BufferedWriter(fstream);
			for(int i = 0 ; i < number ; i++)
			{
				int pervious = random.nextInt(system.size-1);
				out.write(String.valueOf(pervious));
				out.newLine();
				int next    = random.nextInt(system.size-1);
				while(next == pervious)
					next    = random.nextInt(system.size-1);
				out.write(String.valueOf(next));
				out.newLine();
			}
			
			
			out.close();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	}
	
	public static boolean FileCheck() 
	{

		boolean flag = true;	
		  try
			{

			    File myFile = new File("LookupRandomSeed.txt");
				BufferedReader  in = new BufferedReader(new FileReader(myFile));
			} 
			catch (IOException e) 
			{
              //return false;
			    flag = false;
			//	System.out.println("Not find!");
			}
		    

		
		return flag;
	}
	
	
	  public static double meanDistanceToNeighbors(int index)
	  {
		  double mean = 0;
		  int number = 0;
		  for(int i = system.nameIDsize-1  ; i > -1 ; i--)
		  {
			  for(int j = 0 ; j < 2 ; j++)
				  if(nodes.nodeSet[index].getLookup(i, j) != -1)
				  {
					  	mean += nodes.nodeSet[index].Coordination.distance(nodes.nodeSet[nodes.nodeSet[index].getLookup(i, j)].Coordination);
					  	number++;
				  }
				  
		  }
		  
		  return mean/number;
	  }
	  
	  public static double[] DistanceMeans      = new double[system.simRun];
	  public static void meanDistanceEvaluate()
	  {
		  int mean = 0;
		  for(int i = 0 ; i < system.size ; i++)  
		  {
			  mean+= meanDistanceToNeighbors(i);
		  }
		  
			mean /= system.size;
			DistanceMeans[system.simIndex-1] = mean;
			System.out.println("Mean Distance to neighbors " + mean);

			
			if(system.simRun == system.simIndex)
			{
				mean = 0;
				int SD =0;
				for(int i = 0 ; i < system.simRun ; i++)
				      mean += DistanceMeans[i];
				
				mean = mean/system.simRun;
				
				for(int i = 0 ; i < system.simRun ; i++)
				{
				  SD += Math.pow(mean - DistanceMeans[i], 2);	
				  //System.out.println(Means[i]);
				}
				SD = (int) Math.sqrt(SD/system.simRun);
				
				System.out.println("The average distance of each node to it's neighbor is  " + mean + "With the SD " + SD);	
			}
		  
		  
	  }
	  

	    

}
