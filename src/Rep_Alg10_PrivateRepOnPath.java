
import java.util.Random;

import net.sf.javailp.Linear;
import net.sf.javailp.OptType;
import net.sf.javailp.Problem;
import net.sf.javailp.Result;
import net.sf.javailp.Solver;
import net.sf.javailp.SolverFactory;
import net.sf.javailp.SolverFactoryLpSolve;
import lpsolve.*;


public class Rep_Alg10_PrivateRepOnPath 
{

     public static void ReplicateOnPathGenerator()
     {

    	 Random random = new Random();
         int rep = 0 ;
         int searchSource = random.nextInt(system.size - 1);
         int searchDest   = repTools.getM();
         while(rep <=repTools.getMNR() && searchDest <= system.size)
         {
             
	         while(searchSource == searchDest || searchDest >= system.size )
	         {
	             searchDest   = (searchDest + repTools.getM()) % system.size;
	         }
	         
	         //System.out.println(nodes.nodeSet.length + " " + system.size + " " + searchDest);
	         searchDest = nodes.nodeSet[searchDest].getNumID();
	         
	         rep = RepOnPath(searchDest, searchSource, rep);
    	     searchDest = (searchDest + repTools.getM()) % system.size; 
         }
    	 for(int i = 0 ; i < system.size ; i++)
    	 {
    		 int closestReplica = 0;
    		 int closestReplicaDistance = Integer.MAX_VALUE;
    		 for(int j = 0 ; j < system.size ; j++)
    		 {
    			 if(repTools.realWorldReplicaSet[j])
    				 if(nodes.nodeSet[i].Coordination.distance(nodes.nodeSet[j].Coordination) < closestReplicaDistance)
    				 {
    					 closestReplica = j;
    					 closestReplicaDistance = (int) nodes.nodeSet[i].Coordination.distance(nodes.nodeSet[j].Coordination);
    				 }
    		 }
    		 
    		 repTools.realWorldReplicaAssignment[closestReplica][i] = true;
    	 }
    		 
     }
     
     public static int RepOnPath(int num, int startIndex, int RepNum)
     { 
    	 int level = system.nameIDsize-1; //Start from the top
         int next  = -1;
         int before = startIndex;
         if(nodes.nodeSet[startIndex].getLookup(0, 0) == -1 && nodes.nodeSet[startIndex].getLookup(0, 1) == -1)
         { // if only the introducer exists
             return RepNum;
         }
          
         
         else if(nodes.nodeSet[startIndex].getNumID() < num)
         {
         	
         	while(level > 0 && nodes.nodeSet[startIndex].getLookup(level, 1) == -1)
         		level = level - 1;
         	while(level > 0)
         	{

                      if(nodes.nodeSet[startIndex].getLookup(level, 1) != -1)
                      {
                          if(nodes.nodeSet[nodes.nodeSet[startIndex].getLookup(level, 1)].getNumID() > num) 
                              level = level - 1;
                          else 
                              break;
                      }
                      else 
                          level--;

              }
          
              if(level == 0)
              {
            	  return RepNum;
              }
 	         if(level > 0)
 	         {
 	        	 next = nodes.nodeSet[startIndex].getLookup(level, 1);
 	        	 //Replicate here
 	        	 RepNum++;
 	        	 if(RepNum <= repTools.getMNR())
 	        		repTools.realWorldReplicaSet[next] = true;
 	        	 //**************
 	        	 nodes.addTime(next, startIndex);
 	             while(level>=0)
 	             {
 	                     if(nodes.nodeSet[next].getLookup(level, 1) != -1) 
 	                     {
 	                         if(nodes.nodeSet[nodes.nodeSet[next].getLookup(level, 1)].getNumID() <=num)
 	                         {
 	                        	 before = next;
 	                        	 next = nodes.nodeSet[next].getLookup(level, 1);
 	            	        	 //Replicate here
 	            	        	 RepNum++;
 	            	        	 if(RepNum <= repTools.getMNR())
 	            	        		repTools.realWorldReplicaSet[next] = true;
 	            	        	 //**************
 	                        	 nodes.addTime(before, next);
 	                             if(nodes.nodeSet[next].getNumID() == num)
 	                            	return RepNum;
 	                         }
 	                         else
 	                            level = level - 1;   
 	                     }
 	                     else 
 	                         level = level - 1;
 	             }
 	         }
 	         
 	        return RepNum;
         }
         
         else 
         {
          next = -1;
          while(level > 0 && nodes.nodeSet[startIndex].getLookup(level, 0)== -1)
              level = level - 1;
          while(level > 0)
              {
                      if(nodes.nodeSet[startIndex].getLookup(level, 0) != -1)
                      {
                          if(nodes.nodeSet[nodes.nodeSet[startIndex].getLookup(level, 0)].getNumID() < num) 
                              level = level - 1;
                          else 
                              break;
                      }
                      else 
                          level = level - 1;
              }
          
          if(level >= 0)
          {
         	 before = next;
         	 next = nodes.nodeSet[startIndex].getLookup(level, 0);
        	 //Replicate here
        	 RepNum++;
        	 if(RepNum <= repTools.getMNR())
        		 repTools.realWorldReplicaSet[next] = true;
        	 //**************
         	 nodes.addTime(before, next);
              while(level>=0)
              {
                      if(nodes.nodeSet[next].getLookup(level, 0) != -1)
                      {
                          if(nodes.nodeSet[nodes.nodeSet[next].getLookup(level, 0)].getNumID() >= num)
                          {
                         	 before = next;
                         	 next = nodes.nodeSet[next].getLookup(level, 0);
                        	 //Replicate here
                        	 RepNum++;
                        	 if(RepNum <= repTools.getMNR())
                        		 repTools.realWorldReplicaSet[next] = true;
                        	 //**************                        	 
                         	 nodes.addTime(before, next);
                         	 if(nodes.nodeSet[next].getNumID() == num)
                         		return RepNum;
                          }
                          else
                             level = level - 1;   
                      }
                      else 
                          level = level - 1;                                  
              }

          }
          
          return RepNum;
         }
      }
     
         
	 public static void Algorithm()
	 {
	   repTools.reset();
	   double ratio = 0;
	   for(int i = 0; i < repTools.getExperimentNumber() ; i++)
       {
		     repTools.tablesInit();
			 repTools.replicaSetInit();
			 //repTools.replicaSetGenerator(repTools.PrivateOptimizer(repTools.realDistance, repTools.getProblemSize()), "Real", repTools.getProblemSize());
			 ReplicateOnPathGenerator();
			 double localDelay   = repTools.privateAverageDelay(repTools.realWorldReplicaAssignment);
			 //int realDelay    = repTools.privateTotalDelay(repTools.realReplicaAssignment);

			 
		     ratio	  = ratio +  (double)localDelay; ///realDelay;	 
       }	 
	   
	   ratio = ratio / repTools.getExperimentNumber();
	   System.out.println("Average Latency " + ratio);
	   repTools.setRatioDataSet(system.simIndex - 1, ratio);

       if(system.simIndex == system.simRun)
			 {
				//repEvaluation.loadEvaluation();
    	        repTools.evaluation(" Algorithm 10 PrivateRepOnPath "); 
			 }
         
	 }
}