

public class Algorithm2_MatrixAndMean 
{
	 public static int [][] B = new int[system.landmarks][system.landmarks]; //binary matrix
	 public static double []   D = new double[system.nameIDsize]; //Distances to the landmarks
	 public static double []   M = new double[system.nameIDsize]; //Mean of distances to the landmarks
	 public static int nodeIndex = 0; //Number of nodes arriving to the system so far
	 public static String[] nameSpace = new String[256];
	 private static int nameSpaceIndex = 0;
	 public static boolean nameIDGeneration = true;
	 public static String nameID = new String();
	  
	  public static void reset()
	  {
			 B = new int[system.landmarks][system.landmarks]; //binary matrix
			 D = new double[system.nameIDsize]; //Distances to the landmarks
			 M = new double[system.nameIDsize]; //Mean of distances to the landmarks
			 nodeIndex = 0; //Number of nodes arriving to the system so far
			 nameSpace = new String[256];
			 nameSpaceIndex = 0;
			 nameIDGeneration = true;
			 nameID = new String();
			    
	  }
	  
	  public static void nameIDGenerator(int n)
	  {
		        String B;
		    	for(int i = 0; i < Math.pow(2,n); i++)
		        {
		            B = "";
		            int temp = i;
		            for (int j = 0; j < n; j++)
		            {
		                if (temp%2 == 1)
		                    B = '1'+B;
		                else
		                    B = '0'+B;
		                    temp = temp/2;
		            }
		            //System.out.println(B);
		            addToNamespace(B);
		         }
		     
	  }
	  
	  public static void addToNamespace(String B)
	  {
		  nameSpace[nameSpaceIndex] = B;
		  nameSpaceIndex++;
	  }
	  
	  public static void InitializingD()
	  {
		  for(int i = 0 ; i < system.nameIDsize ; i++)
			  D[i] = 0;
	  }
	 
	 public static void InitializingB()
	 {
		 for(int i = 0 ; i < system.landmarks ; i++)
		 {
			 for(int j = 0 ; j < (system.landmarks - i) ; j++)
			 {
				 B[j][i] =  1;
			 }
			 for(int j = system.landmarks - i; j < system.landmarks ; j++)
			 {
				 B[j][i] = 0;
			 }
		 }
		 
	 }
	 
	 public static void PrintB()
	 {
		 for(int i = 0 ; i < system.landmarks ; i++)
		 {
			 for(int j = 0 ; j < system.landmarks ; j++)
			 {
				 System.out.print(B[i][j]);
				 
			 }
			 
			 System.out.println(" ");
		 }
	 }
	 
	 public static void GeneratingD(node n)
	 {
//		 for(int i = 0 ; i < system.nameIDsize ; i++)
//			 D[i] = 0;
		 
		 for(int i = 0 ; i < system.landmarks ; i++)
		 {
			 //for(int j = 0 ; j < system.landmarks ; j++)
			 {
				 D[i] = n.Coordination.distance(Landmark.Set[i]);// * B[i][j];
			 }
		 }
		 System.out.println("D is ");
		 for(int i = 0 ; i < system.landmarks ; i++)
			 System.out.print(D[i] + " ");
		 System.out.println(" ");

		 
		 nodeIndex ++;
	 }
	 
	 public static void UpdatingM()
	 {
		 for(int i = 0 ; i < system.nameIDsize ; i++)
		 {
			 M[i] = M[i] * (nodeIndex - 1) + D[i];
		     M[i] = M[i] / nodeIndex;
		 } 
		 
		 System.out.println("M is ");
		 for(int i = 0 ; i < system.landmarks ; i++)
			 System.out.print(M[i] + " ");
		 System.out.println(" ");
	 }
	 
	 public static void InitilizingM()
	 {
		 for(int i = 0 ; i < system.nameIDsize ; i++)
			 M[i] = 0;
	 }
	 
	 public static String NameIDGenerating()
	 {
		 nameID = new String();
		 for(int i = 0 ; i < system.nameIDsize ; i++)
		 {
			 if(D[i] > M[i])
				 nameID = nameID + "0";
			 else
				 nameID = nameID + "1";
		 }
		 
		 UpdatingM();
		 
		 if(nameSpace[Integer.parseInt(nameID , 2)] == null)
		 {
			 int right = Integer.parseInt(nameID, 2) + 1 ;
			 int left  = Integer.parseInt(nameID, 2) - 1 ;
			 
			 while(true)
			 {
				 if(right < system.size)
				 {
					 if(nameSpace[right] != null)
					 {
						 nameID = nameSpace[right];
						 nameSpace[right] = null;
						 return nameID;
					 }
					 else
						 right++;
				 }
				 if(left >= 0)
				 {
					 if(nameSpace[left] != null)
					 {
						 nameID = nameSpace[left];
						 nameSpace[left] = null;
						 return nameID;
					 }
					 else
						 left--;
				 }
				  
			 }
		 	 
	     }
		 
		 else
		 {
		     nameID = nameSpace[Integer.parseInt(nameID, 2)];
		     nameSpace[Integer.parseInt(nameID, 2)] = null;
		     return nameID;
		 }
	 }
	 
	 
	 public static String Algorithm(node n)
	 {
	  if(nameIDGeneration)
	  {
			  nameIDGenerator(system.nameIDsize);
			  nameIDGeneration = false;
			  InitializingB();
			  InitilizingM();
			  PrintB();
	  }

		 InitializingD();
		 GeneratingD(n);
		 
		 return NameIDGenerating();
	 }
	 
	 
	 
	 
	 
	 
	 

}
