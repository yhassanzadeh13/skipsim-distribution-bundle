import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

class pointChance
{
    public Point p = new Point();
    public double upperChance = 0;
    public double lowerChance = 0;
    public double Chance = 0;
    public double prob = 0;
}
public class TopologyGenerator
{
  public static ArrayList<pointChance> pointSeed = new ArrayList<pointChance>(); 
  public static boolean seedInitialization = true;
  public static boolean[][] pointChance = new boolean[system.domainSize][system.domainSize];
  public static double [][] pointProbability = new double[system.domainSize][system.domainSize];
  public static double totalChance = 0;
  public static double chanceIndex = 0;
  public static double maxProb = 0;
  
  
  public static void reset()
  {
	  pointSeed = new ArrayList<pointChance>(); 
	  seedInitialization = true;
	  pointChance = new boolean[system.domainSize][system.domainSize];
	  pointProbability = new double[system.domainSize][system.domainSize];
	  chanceIndex = 0;
	  totalChance = 0;
  }
  
  public static node UniformRandomNodeGenerator()
  {
	    Random random = new Random();
	    node n = new node();
        double sumOfDistancesToTheLandmarks = 0;
	 while(true)
	 {
		n.Coordination.x = random.nextInt(system.domainSize);
		n.Coordination.y = random.nextInt(system.domainSize);
		for(int i = 0 ; i < Landmark.Set.length ; i++)
	    {

	    	sumOfDistancesToTheLandmarks += n.Coordination.distance(Landmark.Set[i]);
			if(n.Coordination.distance(Landmark.Set[i]) < system.UniformLandmarkThereshould)
			{
	    		nodes.addNodeToSet(n);
	    		return n;
			}
	    }
		
		//System.out.println(sumOfDistancesToTheLandmarks);
		if(sumOfDistancesToTheLandmarks < system.UniformSumOfLandmarkThereshould)
		{
			nodes.addNodeToSet(n);
			return n;
		}
		

	 }
	    
  }
  

  
  public static node LandmarkBasedSeedRandomNodeGenerator()
  {

	    Random random = new Random();
	    node n = new node();
	    if(seedInitialization)
	    {
	    	seedInitialization = false;
	    	probInit();
	    	pointChanceInit();
	    }
	    
        while(true)
        {

        	int index = random.nextInt(1000000);

        	
	        for(int ii = 0 ; ii < pointSeed.size() ; ii++)
	        {
	        	//System.out.println("down " + pointSeed.get(ii).lowerChance +"  up " + pointSeed.get(ii).upperChance  + " index " + index + " nodes " + nodes.nodeIndex);
	        	if(pointSeed.get(ii).lowerChance <= index 
	        			&&
	        			pointSeed.get(ii).upperChance >= index
	        			&&
	        			pointChance[pointSeed.get(ii).p.x][pointSeed.get(ii).p.y])
	        	{
					n.Coordination.x = pointSeed.get(ii).p.x;
					n.Coordination.y = pointSeed.get(ii).p.y;
					pointChance[n.Coordination.x][n.Coordination.y] = false;
					return n;
	        	}
	        	


	        		
	        }
        }
    
  }
  
  public static void probInit()
  {

	  for(int i = 0 ; i < system.domainSize ; i+= 5)
	  {
		  for(int j = 0 ; j < system.domainSize ; j+= 5)
		  {

			      pointChance p = new pointChance();
				  p.p.x = i;
				  p.p.y = j;
	
				  for(int k = 0 ; k < system.landmarks ; k++)
				  {
					  if(p.p.distance(Landmark.Set[k]) < system.domainSize / 6)
					  {
							  p.Chance = 1000000*system.landmarks;
							  //System.out.println("Super " + p.Chance);
							  continue;
					  }
					  else
					  {
						  p.Chance += (1- (p.p.distance(Landmark.Set[k]) / (system.domainSize*1.4)));
					  }
				  }
				  
				  
				  
				  totalChance += p.Chance;
				  pointSeed.add(p);
		    }

		  
		  
	  }
	  

	  for(int i = 0 ; i < pointSeed.size() ; i++)
	  {

		  pointSeed.get(i).prob        = (pointSeed.get(i).Chance / totalChance)*1000000;
		  if(pointSeed.get(i).prob > maxProb)
			  maxProb =  (pointSeed.get(i).prob);
			  

		  pointSeed.get(i).lowerChance = chanceIndex;
		  chanceIndex += pointSeed.get(i).prob ;
		  pointSeed.get(i).upperChance = pointSeed.get(i).lowerChance + pointSeed.get(i).prob;

	  }

	  
  }
  
  public static void pointChanceInit()
  {
	  for(int i = 0 ; i < system.domainSize  ; i++)
		  for(int j = 0 ; j <system.domainSize ; j++)
		  {
			  pointChance[i][j] = true;
		  }
  }
  

  
  

}
