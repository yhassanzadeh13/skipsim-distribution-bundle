import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;


public class capture
{
	
   public static void saveAsJGP()
   {      
        try
        {
            
            Robot robot = new Robot();
            // Capture the screen shot of the area of the screen defined by the rectangle
            BufferedImage bi=robot.createScreenCapture(new Rectangle(515,535));
            ImageIO.write(bi, "jpg", new File("imageTest "+ system.simIndex +".jpg"));
            System.out.println("System saved the image");
            
        } 
        catch (AWTException e) 
        {
            e.printStackTrace();
        }
        catch (IOException e) 
        {
            e.printStackTrace();
        }
    }
}


