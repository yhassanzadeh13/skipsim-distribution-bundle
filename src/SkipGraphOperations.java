
public class SkipGraphOperations
{
    
    public static int commonBits(String s1, String s2)
    {
	
		  int k = 0;
		  while(s1.charAt(k) == s2.charAt(k))
		  {
			 k++;
		     if(k >= s1.length() || k >= s2.length() || k >= system.nameIDsize-1 )
		    	 break;
		  }
			  
		  return k;
	            
    }
    
    
    
	public static void Insert(int index)
	{

            //System.out.println("Insert started!");
		    int Left = -1;
            int Right= -1;
           

            int before = SearchByNumID(nodes.nodeSet[index].getNumID(), nodes.nodeSet[index].introducer);//gets the port of this node's NumID
            //System.out.println("Search by num for   + nodes.nodeSet[index].getNumID() + " is " + before + "  Num " + nodes.nodeSet[before].getNumID());

            
           
            Left = before;
//            if(before != -1)
            Right = nodes.nodeSet[before].getLookup(0, 1, index);
//            else
//               Right = nodes.nodeSet[0].lookup[0][1];
            
            //nodes.nodeSet[index].lookup[0][0] = before;
            nodes.nodeSet[index].setLookup(0, 0, before, index);
//            if(before != -1)
            //nodes.nodeSet[before].lookup[0][1] = index;
            nodes.nodeSet[before].setLookup(0, 1, index, index);
//            else
//            	nodes.nodeSet[0].lookup[0][0] = index;
            
            //nodes.nodeSet[index].lookup[0][1] = Right;
            nodes.nodeSet[index].setLookup(0, 1, Right, index);            
            if(Right != -1)
            	//nodes.nodeSet[Right].lookup[0][0] = index;
            	nodes.nodeSet[Right].setLookup(0, 0, index, index);   

                       
            int level = 0;
            while(true)
            {
               while(true)
               {
                    if(Left != -1)
                    {
                       if(commonBits(nodes.nodeSet[Left].nameID, nodes.nodeSet[index].nameID) <= level)
                            //Left = nodes.nodeSet[Left].lookup[level][0];
                    	    Left = nodes.nodeSet[Left].getLookup(level, 0, index);
                        else 
                            break;
                    }
                            
                    if(Right != -1)
                    {
                         if(commonBits(nodes.nodeSet[Right].nameID, nodes.nodeSet[index].nameID) <= level)
                    		 //Right = nodes.nodeSet[Right].lookup[level][1];
                        	 Right = nodes.nodeSet[Right].getLookup(level, 1, index);
                         else 
                            break;
                    }
                    if(Right == -1 && Left == -1)
                        break;
                    
                }
                
                if(Left!= -1)
                {
                	if(commonBits(nodes.nodeSet[Left].nameID, nodes.nodeSet[index].nameID) > level)
                    {
                        
                        //int RightNeighbor = nodes.nodeSet[Left].lookup[level+1][1];
                        int RightNeighbor = nodes.nodeSet[Left].getLookup(level+1, 1, index);
                        
                        //nodes.nodeSet[Left].lookup[level+1][1] = index;
                        nodes.nodeSet[Left].setLookup(level+1, 1, index, index);
                        if(RightNeighbor != -1)
                        	//nodes.nodeSet[RightNeighbor].lookup[level+1][0] = index;
                        	nodes.nodeSet[RightNeighbor].setLookup(level+1, 0, index, index);

                        //nodes.nodeSet[index].lookup[level+1][0] = Left;
                        //nodes.nodeSet[index].lookup[level+1][1] = RightNeighbor;
                        nodes.nodeSet[index].setLookup(level+1, 0, Left, index);
                        nodes.nodeSet[index].setLookup(level+1, 1, RightNeighbor, index);                        
                        Right = RightNeighbor;
                        level++; //Has to add to DS version
                    }
                
                }
                
                else if(Right != -1)
                {
                    if(commonBits(nodes.nodeSet[Right].nameID, nodes.nodeSet[index].nameID) > level)
                    {
                        int LeftNeighbor = -1;
                        //LeftNeighbor = nodes.nodeSet[Right].lookup[level+1][0];
                        LeftNeighbor = nodes.nodeSet[Right].getLookup(level+1, 0, index);
                        
                        //nodes.nodeSet[Right].lookup[level+1][0] = index;
                        nodes.nodeSet[Right].setLookup(level+1, 0, index, index);
                        if(LeftNeighbor != -1)
                        	//nodes.nodeSet[LeftNeighbor].lookup[level+1][1] = index;
                        	nodes.nodeSet[LeftNeighbor].setLookup(level+1, 1, index, index);
                        
                        //nodes.nodeSet[index].lookup[level+1][0] = LeftNeighbor;
                        //nodes.nodeSet[index].lookup[level+1][1] = Right;
                        nodes.nodeSet[index].setLookup(level+1, 0, LeftNeighbor, index);
                        nodes.nodeSet[index].setLookup(level+1, 1, Right, index);                           
                        Left = LeftNeighbor;
                        level++; //Has to add to the DS version
                    }
                }
                
          
                
                    
               //level++;   Has to remove from DS version  
               if(level >= system.nameIDsize)
                   break;
               if(Left == -1 && Right == -1)
                   break;
            }
            //System.out.println("Insert finished");
            //for(int i = 0 ; i <= index ; i++)
            //      nodes.nodeSet[index].printLookup();
    }
    
    
        
    public static int SearchByNameID(String name, int startIndex)
    {

            
    	    System.out.println("Search for name id starts from " + startIndex + " " + nodes.nodeSet[startIndex].nameID + " for " + name);
    	    int Left = 0;
            int Right= 0;
            int level = 0;
            int before = startIndex;
//            boolean rightFlag = false;
//            boolean leftFlag  = false;
            
            
            if(nodes.nodeSet[startIndex].nameID == name)  // UPDATED!!!!
            	return startIndex;
                      
//            Left  = nodes.nodeSet[startIndex].lookup[0][0];
//            Right = nodes.nodeSet[startIndex].lookup[0][1];
            //Left  = nodes.nodeSet[startIndex].getLookup(0, 0, startIndex);
            //Right = nodes.nodeSet[startIndex].getLookup(0, 1, startIndex);            
              Left  = getResolve(startIndex, 0, 0);
              Right = getResolve(startIndex, 0, 1);
            if(commonBits(nodes.nodeSet[startIndex].nameID,name) > level)
            {//goes to upper levels for search
                 level = commonBits(nodes.nodeSet[startIndex].nameID , name);
//                 Left  = nodes.nodeSet[startIndex].lookup[level][0];
//                 Right = nodes.nodeSet[startIndex].lookup[level][1]; 
                 //Left  = nodes.nodeSet[startIndex].getLookup(level, 0, startIndex);
                 //Right = nodes.nodeSet[startIndex].getLookup(level, 1, startIndex);
                 Left = getResolve(startIndex, level, 0);
                 Right = getResolve(startIndex, level, 1);
                 
                 
            }
            
            
            

               
               while(true)
               {
                    if(Left != -1)
                    {
 //                   	leftFlag = true; 
                        if(nodes.nodeSet[Left].nameID == name)
                            return Left;
                        else if(commonBits(nodes.nodeSet[Left].nameID,name) <= level)
                        {
                            //Left  = nodes.nodeSet[Left].lookup[level][0];
                             //Left  = nodes.nodeSet[Left].getLookup(level, 0, startIndex);
                            before = Left; 
                        	Left   = getResolve(Left, level, 0);
                        	nodes.addTime(Left, before);
                        }
                        else if(commonBits(nodes.nodeSet[Left].nameID,name) > level)
                        {
                            level = commonBits(nodes.nodeSet[Left].nameID,name);
                           // Right = nodes.nodeSet[Left].lookup[level][1];
                           // Left  = nodes.nodeSet[Left].lookup[level][0];
                            //Right = nodes.nodeSet[Left].getLookup(level, 1, startIndex); 
                            //Left  = nodes.nodeSet[Left].getLookup(level, 0, startIndex);
                            before = Left;
                            Right = getResolve(Left, level, 1);
                            Left  =  getResolve(Left, level, 0);
                            nodes.addTime(Left, before);
                            nodes.addTime(Right, before);
                            continue;
                        }
//                        leftFlag = false;
                    }        
                    
                    else if(Right != -1)
                    {
//                    	rightFlag = true;
                        if(nodes.nodeSet[Right].nameID == name)
                            return Right;
                        else if(commonBits(nodes.nodeSet[Right].nameID,name) <=level)
                        {
                         //Right = nodes.nodeSet[Right].lookup[level][1];
                         //	Right = nodes.nodeSet[Right].getLookup(level, 1, startIndex);
                        	before = Right;
                        	Right = getResolve(Right, level, 1);
                        	nodes.addTime(Right, before);
                        }
                        else if(commonBits(nodes.nodeSet[Right].nameID,name) > level)
                        {
                            level = commonBits(nodes.nodeSet[Right].nameID,name);
                            //Right = nodes.nodeSet[Right].lookup[level][1];
                            //Left  = nodes.nodeSet[Right].lookup[level][0];  
                            before = Right;
                            Right = getResolve(Right, level, 1);
                            Left =  getResolve(Right, level, 0);
                            nodes.addTime(Left, before);
                            nodes.addTime(Right, before);                            
                            continue;
                        }
//                        rightFlag = false;
                    }
                    if(Right == -1 && Left == -1)
                        break;
                }

        
        
         
        return -1; 
         
    } 
    
    public static int SearchByNumID(int num, int startIndex)
    {
        int level = system.nameIDsize-1; //Start from the top
        int next  = -1;
        int before = startIndex;
        //boolean nextFlag = false;
        if(nodes.nodeSet[startIndex].getLookup(0, 0) == -1 && nodes.nodeSet[startIndex].getLookup(0, 1) == -1)
        { // if only the introducer exists
            return startIndex;
        }
         
        
        else if(nodes.nodeSet[startIndex].getNumID() < num)
        {
        	
        	while(level > 0 && nodes.nodeSet[startIndex].getLookup(level, 1) == -1)
        		level = level - 1;
        	while(level > 0)
        	{

                     if(nodes.nodeSet[startIndex].getLookup(level, 1) != -1)
                     {
                         if(nodes.nodeSet[nodes.nodeSet[startIndex].getLookup(level, 1)].getNumID() > num) 
                             level = level - 1;
                         else 
                             break;
                     }
                     else 
                         level--;

             }
         
             if(level == 0)
             {
            	 return startIndex;
             }
	         if(level > 0)
	         {
	        	 next = nodes.nodeSet[startIndex].getLookup(level, 1);
	        	 nodes.addTime(next, startIndex);
	             while(level>=0)
	             {
	                     if(nodes.nodeSet[next].getLookup(level, 1) != -1) 
	                     {
	                         if(nodes.nodeSet[nodes.nodeSet[next].getLookup(level, 1)].getNumID() <=num)
	                         {
	                        	 before = next;
	                        	 next = nodes.nodeSet[next].getLookup(level, 1);
	                        	 nodes.addTime(before, next);
	                             if(nodes.nodeSet[next].getNumID() == num)
	                                                                   return next;
	                         }
	                         else
	                            level = level - 1;   
	                     }
	                     else 
	                         level = level - 1;
	             }
	         }
	         
	         return next;
        }
        
        else 
        {
         next = -1;
         while(level > 0 && nodes.nodeSet[startIndex].getLookup(level, 0)== -1)
             level = level - 1;
         while(level > 0)
             {
                     if(nodes.nodeSet[startIndex].getLookup(level, 0) != -1)
                     {
                         if(nodes.nodeSet[nodes.nodeSet[startIndex].getLookup(level, 0)].getNumID() < num) 
                             level = level - 1;
                         else 
                             break;
                     }
                     else 
                         level = level - 1;
             }
         
         if(level >= 0)
         {
        	 before = next;
        	 next = nodes.nodeSet[startIndex].getLookup(level, 0);
        	 nodes.addTime(before, next);
        	// nodes.nodeSet[startIndex].printLookup();
             while(level>=0)
             {
//            	 if(next != -1)
//            	 {
                     if(nodes.nodeSet[next].getLookup(level, 0) != -1)
                     {
                         if(nodes.nodeSet[nodes.nodeSet[next].getLookup(level, 0)].getNumID() >= num)
                         {
                        	 before = next;
                        	 next = nodes.nodeSet[next].getLookup(level, 0);
                        	 nodes.addTime(before, next);
                        	 if(nodes.nodeSet[next].getNumID() == num)
                                                                      return next;
                         }
                         else
                            level = level - 1;   
                     }
                     else 
                         level = level - 1;
//            	 }
            	 

                                 
             }

         }
         
         return next;
        }
     }
    
    public static int getResolve(int dst, int i, int j)
    {
    	if(dst == -1)
    		return -1;
    	if(nodes.nodeSet[dst].getLookup(i, j) == -1)
    		return -1;
    	else 
    	{
    		if(nodes.nodeSet[nodes.nodeSet[dst].getLookup(i, j)].isDeactive() == false)
    	        return (nodes.nodeSet[dst].getLookup(i, j));
    		else
    		{
    			if(system.backup)
		    		{
		    			if(nodes.nodeSet[dst].getBackup(i, j) == -1)
		    				return -1;
		    			else
		    			{
		    				if(nodes.nodeSet[nodes.nodeSet[dst].getBackup(i, j)].isDeactive() == false)
		    					return (nodes.nodeSet[dst].getBackup(i, j));
		    				else
		    					return -1;
		    			}
		    					
		    		}
    			else
    			{
    				RecoveryEvaluation.failTransCount++;
    				return -1;
    			}
    		}
		 }
    	
    }
    
  
        
}
