import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;


public class node 
{
  String nameID = new String();
  Point Coordination = new Point();
  private int numID ;//Number ID is assumed to be greater than 0 
  private int lookup[][] = new int[system.nameIDsize][2];//We assume that we have 4 levels 
  private int backup[][] = new int[system.nameIDsize][2];
//  public static String backup[][] = new String[4][2]; //We assume that we have 4 levels
  double batteryLevel = 100; 
  final int thershold = 3;//if temprature exceeds this amount it will be counted high-visited 
  int size = 4;
//  boolean nextFlag = false;//It is for searching by numID
//  boolean leftFlag = false;//It is for searching by numID
//  boolean rightFlag = false;//It is for searching by numID
//  int next =  0;//elements of lookup table
//  int Left =  0;//elements of lookup table 
//  int Right = 0;//elements of lookup table
  int introducer = 0;
  int address    = 0;
  private boolean hotSpot = false;
  private boolean failure = false;
  private boolean backupTicket  = true;
  
  public node()
  {
	 if(nodes.nodeIndex == 0)
		 numID = 0;
	 else
	 {
	    do
	    {
		  Random random = new Random();
		  numID = random.nextInt(200);
	    }while(numID == 0);
	 }
	  for(int i = 0 ; i < system.nameIDsize ; i++)
		  for(int j = 0 ; j < 2 ; j++)
		  {
			  lookup[i][j] = -1;
			  backup[i][j] = -1;
		  }
  }
  
  public int getLookup(int i, int j)
  {
	  return lookup[i][j];
  }
  
  public void setLookup(int i, int j, int address)
  {
	  lookup[i][j] = address;
  }
  
  public int getLookup(int i, int j, int startIndex)
  {
	  //System.out.println("getLookup receives " + i + " " + j);
	 // system.totalRTTDelay += Coordination.distance(nodes.nodeSet[startIndex].Coordination);
	 // nodes.timeIncrease((int) Coordination.distance(nodes.nodeSet[startIndex].Coordination));
	  batteryLevelUpdate(Coordination.distance(nodes.nodeSet[startIndex].Coordination));
	  return lookup[i][j];
  }
  
  public void setLookup(int i, int j, int address, int startIndex)
  {
	 // system.totalRTTDelay += Coordination.distance(nodes.nodeSet[startIndex].Coordination);
	  //nodes.timeIncrease((int) Coordination.distance(nodes.nodeSet[startIndex].Coordination));
	  batteryLevelUpdate(Coordination.distance(nodes.nodeSet[startIndex].Coordination));
	  lookup[i][j] = address;
  }
  
  public int getBackup(int i, int j)
  {
	  return backup[i][j];
  }
  
  public void setBackup(int i, int j, int address)
  {
	  backup[i][j] = address;
  }
  
  public int getBackup(int i, int j, int startIndex)
  {
	 // system.totalRTTDelay += Coordination.distance(nodes.nodeSet[startIndex].Coordination);
	  //nodes.timeIncrease((int) Coordination.distance(nodes.nodeSet[startIndex].Coordination));
	  backupBatteryLevelUpdate(Coordination.distance(nodes.nodeSet[startIndex].Coordination));
	 // System.out.println("GetBackup");
	  return backup[i][j];

  }
  
  public void setBackup(int i, int j, int address, int startIndex)
  {
	 // system.totalRTTDelay += Coordination.distance(nodes.nodeSet[startIndex].Coordination);
	 // nodes.timeIncrease((int) Coordination.distance(nodes.nodeSet[startIndex].Coordination));
	  backupBatteryLevelUpdate(Coordination.distance(nodes.nodeSet[startIndex].Coordination));
	//  backup[i][j] = address;
	  System.out.println("SetBackup");
  }
  

  
  public void setNumID()
  {
	  Random random = new Random();
	  numID = random.nextInt(200); 
  }
  
  public int getNumID()
  {
	  return numID;
  }
  
  public void batteryLevelUpdate(double usage)
  {
	if(nodes.nodeIndex == system.size)
	{
	      if(batteryLevel > 1) 
	      {
	    	  batteryLevel -= (usage/system.domainSize);
	          //System.out.println(usage/system.screenSize);
	      }
	      else 
	    	  batteryLevel = 0;

	 if(system.backup)
	 {
		  if(batteryLevel <= 0)
			  failure = true;
		  if(hotSpotCheck() && backupTicket)
			  callBackup();
	 }
	}
  }
  
  public void backupBatteryLevelUpdate(double usage)
  {
	if(nodes.nodeIndex == system.size)
	{
	      if(batteryLevel > 1) 
	      {
	    	  batteryLevel -= (usage/system.domainSize);
	          //System.out.println(usage/system.screenSize);
	      }
	      else 
	    	  batteryLevel = 0;
		  if(batteryLevel <= 0)
			  failure = true;
	}
  }
  
  public boolean isDeactive()
  {
	  return failure;
  }
  public boolean hotSpotCheck()
  {
	  if(hotSpot)
		  return true;
	  else
		  if(batteryLevel < system.criticalBatteryLevel)
		  {
			  hotSpot = true;
			  return true;
		  }
		  else
			  return false;
  }
  
  public boolean callBackup()
  {
      int k = 0;  
      boolean flag = false;
      for (int i = 0; i < system.nameIDsize; i++)
            for(int j = 0; j < 2; j++)
            {
                if (j == 0)
                   k = 1;
                
                else 
                   k = 0;
                 
                if(lookup[i][j] != -1)
                {
                   nodes.nodeSet[lookup[i][j]].setBackup(i, k, lookup[i][k], address);
                   flag=true;
                }
                
            }
      backupTicket = false;
      return (flag);
    
  }
  
  public void energyReset()
  {
	  batteryLevel = 100;
	  hotSpot = false;
	  failure = false;
	  backupTicket = true;
	  for(int i = 0 ; i < system.nameIDsize ; i++)
		  for(int j = 0 ; j < 2 ; j++)
		  {
			  backup[i][j] = -1;
		  }
  }
  
  public void printLookup()
  {
	  System.out.println("NameID " + nameID + " NumID " + numID);
	  for(int i = system.nameIDsize-1  ; i > -1 ; i--)
	  {
		  for(int j = 0 ; j < 2 ; j++)
			  System.out.print(lookup[i][j] + "    ");
		  
		  System.out.println(" ");
	  }
  }
  
  public void printLookupNameID()
  {
	  System.out.println("NameID " + nameID + " NumID " + numID);
	  for(int i = system.nameIDsize-1  ; i > -1 ; i--)
	  {
		  for(int j = 0 ; j < 2 ; j++)
			  if(lookup[i][j] != -1)
				  	System.out.print(nodes.nodeSet[lookup[i][j]].nameID + "    ");
			  else 
				  System.out.print(-1);
		  
		  System.out.println(" ");
	  }
  }
  
  public int neighborNumber()
  {
	  ArrayList<Integer> neighbors = new ArrayList<Integer>();
	  for(int i = 0 ; i < system.nameIDsize ; i++)
		  for(int j = 0 ; j < 2 ; j++)
		  {
			  if(lookup[i][j] != -1 && !neighbors.contains(lookup[i][j]))
				  neighbors.add(lookup[i][j]);
		  }
	  
	  return neighbors.size();
  }
  
  

  
  
  
}

