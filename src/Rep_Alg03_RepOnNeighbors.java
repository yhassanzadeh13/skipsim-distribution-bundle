
import java.util.Random;

import net.sf.javailp.Linear;
import net.sf.javailp.OptType;
import net.sf.javailp.Problem;
import net.sf.javailp.Result;
import net.sf.javailp.Solver;
import net.sf.javailp.SolverFactory;
import net.sf.javailp.SolverFactoryLpSolve;
import lpsolve.*;


public class Rep_Alg03_RepOnNeighbors 
{

     public static void ReplicateOnNeighborsGenerator()
     {

         
    	 int index = 0;
         while(index < system.size && nodes.nodeSet[index].neighborNumber() < repTools.getMNR())
        	 index++;
         
         if(index >= system.size)
         {
        	 System.out.println("Error in Rep_Alg03_RepOnNeighbors.java: there is no node in the system with " + repTools.getMNR() + " neighbors"
        	 		+ "\n Change MNR please");
        	 System.exit(0);
         }
         
   	    for(int i = 0, rep = 0 ; i < system.nameIDsize && rep <= repTools.getMNR() ; i++)
		  for(int j = 0 ; j < 2 ; j++)
		  {
			  if(nodes.nodeSet[index].getLookup(i, j) != -1 )
			  {
				  repTools.realWorldReplicaSet[nodes.nodeSet[index].getLookup(i, j)] = true;
				  rep ++;
			  }
				  
		  }
    	 
    	 
    	 for(int i = 0 ; i < system.size ; i++)
    	 {
    		 int closestReplica = 0;
    		 int closestReplicaDistance = Integer.MAX_VALUE;
    		 for(int j = 0 ; j < system.size ; j++)
    		 {
    			 if(repTools.realWorldReplicaSet[i])
    				 if(nodes.nodeSet[i].Coordination.distance(nodes.nodeSet[j].Coordination) < closestReplicaDistance)
    				 {
    					 closestReplica = i;
    					 closestReplicaDistance = (int) nodes.nodeSet[i].Coordination.distance(nodes.nodeSet[j].Coordination);
    				 }
    		 }
    		 
    		 repTools.realWorldReplicaAssignment[closestReplica][i] = true;
    	 }
    		 
     }
	 
	 public static void Algorithm()
	 {
		   repTools.reset();
		   double ratio = 0;
		   for(int i = 0; i < repTools.getExperimentNumber() ; i++)
	       { 
			 repTools.tablesInit();
			 repTools.replicaSetInit();

			 repTools.replicaSetGenerator(repTools.PublicOptimizer(repTools.realDistance, system.size), "Real", system.size);
			 ReplicateOnNeighborsGenerator();
			 int localDelay = repTools.publicTotalDelay(repTools.realWorldReplicaAssignment);
			 int realDelay  = repTools.publicTotalDelay(repTools.realReplicaAssignment);

			 
			 ratio = ratio +  (double)localDelay/realDelay;
	       } 
		 ratio = ratio / repTools.getExperimentNumber();
		 System.out.println("Ratio " + ratio);
		 repTools.setRatioDataSet(system.simIndex - 1, ratio);
		 
		 if(system.simIndex == system.simRun)
		 {
			 //repEvaluation.loadEvaluation();
			 repTools.evaluation(" Algorithm 03 RepOnNeighbors ");

		 }
         
	 }
}