import java.awt.Point;
import java.util.Random;


public class Algorithm13_RegionSubRegion 
{
  public static String[] nameSpace = new String[system.size];
  private static int nameSpaceIndex = 0;
  public static boolean nameIDGeneration = true;
  
  
      public static void reset()
      {
    	  nameSpace = new String[system.size];
    	  nameSpaceIndex = 0;
    	  nameIDGeneration = true;
      }
	  public static void nameIDGenerator(int n)
	  {
		        String B;
		    	for(int i = 0; i < Math.pow(2,n); i++)
		        {
		            B = "";
		            int temp = i;
		            for (int j = 0; j < n; j++)
		            {
		                if (temp%2 == 1)
		                    B = '1'+B;
		                else
		                    B = '0'+B;
		                    temp = temp/2;
		            }
		            //System.out.println(B);
		            addToNamespace(B);
		         }
		     
	  }
	  
	  public static void addToNamespace(String B)
	  {
		  if(nameSpaceIndex < system.size)
		  {
			  nameSpace[nameSpaceIndex] = B;
			  nameSpaceIndex++;
		  }
	  }
	  
	  public static String RandomNameIDAssignment(node n)
	  {  
		  Random random = new Random();
		  int index = random.nextInt(system.size-1);
		  
		  int counter = 0;
		  while(nameSpace[index] == null && counter < 100)
		  {
		      index = random.nextInt(system.size-1);
		      counter ++;
		  }
		  
		  if(counter >= 1000000)
			  for(int i = 0 ; i < system.size ; i++)
			  {
				  if(nameSpace[i] != null)
				  {
					  index = i;
					  break;
				  }
				  
			  }
		  
		  String nameID = nameSpace[index];
		  nameSpace[index] = null;
          nameID = findRegion(n.Coordination) + nameID; 
          
		return nameID;
	}
    
	  
	public static String Algorithm(node n)
	{
		  if(nameIDGeneration)
		  {
			  nameIDGenerator(system.landmarks);
			  nameIDGeneration = false;
		  }
		  
		  return RandomNameIDAssignment(n);
	}
	
    static String findRegion(Point p)
    {
    	if(p.x < system.domainSize * 0.5)
    	{
    		if(p.y < system.domainSize * 0.25)
    			return "000";
    		else if(p.y < system.domainSize * 0.5)
    			return "001";
    		else if(p.y < system.domainSize *0.75)
    			return "010";
    		else
    			return "011";
    	}
    	
    	else
    	{
    		if(p.y < system.domainSize * 0.25)
    			return "100";
    		else if(p.y < system.domainSize * 0.5)
    			return "101";
    		else if(p.y < system.domainSize *0.75)
    			return "110";
    		else
    			return "111";    		
    	}
    }
}