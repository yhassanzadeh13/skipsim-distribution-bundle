import java.awt.Point;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;



//This class provide a uniform simulation for all the algorithms
//All the nodes for n times of iteration create once and then stores in sim_x.txt 
//Which x is the number of the simulation. After creation of the nodes information
//For the further simulation it wouldn't create any other nodes and it would just load 
//The nodes from the sim_x.txt
public class Simulator
{

	public static void main(String[] args)
	{  
		//Reading, configuring and showing the config file
		fileInteractions.readConfigFile();
		fileInteractions.showTheConfig();
    	
		
	    while(system.simIndex < system.simRun)
	    { 
	    	 system.simIndex ++;
    	     system.reset();
	         Landmark.reset();
	         nodes.reset();
	         TopologyGenerator.reset();

    	     
    	     ClosestNode.externalReset();
    	     
    	     nameIDAssignmentReset();
    	     if(FileCheck())
        	 {
    	    	 
        			 try
        			 { 
        				 BufferedReader  in	 = new BufferedReader(new FileReader("topologies\\" + system.size + "\\" + system.size + "_sim_"+ system.simIndex + ".txt")); 
						 for(system.index = 0;system.index < system.landmarks ; system.index++) 
						 {

				            	 Point p = new Point();
				            	 p.x = Integer.parseInt(in.readLine());
				            	 p.y = Integer.parseInt(in.readLine());
				                 Landmark.Set[system.index] = p;
		                 

						 }

	                	for(int i = 0 ; i < system.size ; i++)
	                	{
	                		node n = new node();
	                		n.Coordination.x = Integer.parseInt(in.readLine());
	                		n.Coordination.y = Integer.parseInt(in.readLine());
	                		n.nameID = nameIDAssignment(n);
	                		if(n.nameID == null)
	                			system.dynamicNameID = false;
	                		//System.out.println("Node name id is " + n.nameID + " numerical id is " + n.getNumID() + " system index =  " + i);

	                		nodes.addNodeToSet(n);   						            	 

	                	}
		                
	                	
	                	in.close();
					 } 
        			 catch (FileNotFoundException e) 
        			 {
						// TODO Auto-generated catch block
						e.printStackTrace();
					 }
        			 catch (NumberFormatException e)
        			 {
						// TODO Auto-generated catch block
						e.printStackTrace();
					 } 
        			 catch (IOException e) 
        			 {
						// TODO Auto-generated catch block
						e.printStackTrace();
					 }       			 
        			
        		 }

	              
        	
        	 
    	     else 
        	 {
				 System.out.println("Generating the landmarks: ");
    	    	 for(system.index = 0; system.index < system.landmarks  ; system.index++) 
				 {
	            	 Random random = new Random();
	            	 Point p = new Point();
	            	 p.x = random.nextInt((int)(0.8 * system.domainSize));
	            	 p.y = random.nextInt((int)(0.8 * system.domainSize));
	                 Landmark.Set[system.index] = p;
	                 //System.out.println("Landmark" + system.index + "x = " + p.x + "y = " + p.y);	
				 }
				 
            	for(int i = 0 ; i < system.size ; i++)
            	{
            		node n = new node();
            		if(system.nodeGeneration.equals("landmark"))
            			n = TopologyGenerator.LandmarkBasedSeedRandomNodeGenerator();
            		else
            			n = TopologyGenerator.UniformRandomNodeGenerator();
            		n.nameID = nameIDAssignment(n);
            		if(n.nameID == null)
            			system.dynamicNameID = false;
            		//System.out.println("Node name id is " + n.nameID + " numerical id is " + n.getNumID() + " system index =  " + i);
            		nodes.addNodeToSet(n);
            	}
            	
        	 }        
        		  

        			  
    	     	if(system.dynamicNameID == false)
    	     	{
    	     		node n = new node();
    	     		nameIDAssignment(n);
    	     	} 

    	     	replication();
    	     	
    	     	if(system.searchByNumericalID > 0)
    	     	{
	  	          LookupEvaluation.evaluator(system.searchByNumericalID, "numID");
				  LookupEvaluation.meanDistanceEvaluate();
    	     	}
    	     	
    	     	if(system.searchByNameID > 0)
    	     	{
	  	          LookupEvaluation.evaluator(system.searchByNameID, "nameID");
				  LookupEvaluation.meanDistanceEvaluate();
    	     	}
    	     	
    	     	if(system.idEvaluation)
    	     	{
    	     		idEvaluation.maltabRepAutoMeanSDEvaluation();
    	     	}
    	     		
		          if(!FileCheck())
				  {
					  SaveToFile();
				  }
	    }
                	
 }		
      
	
	
	//Check that whether the sim_x files are ready or not
	//if they were ready it will return true else false
	public static boolean FileCheck() 
	{

		boolean flag = true;	
		  try
			{
				File myFile = new File("topologies\\" + system.size + "\\" + system.size+ "_sim_"+ system.simIndex + ".txt");
				BufferedReader  in = new BufferedReader(new FileReader(myFile));
			} 
			catch (IOException e) 
			{
              //return false;
			    flag = false;
			//	System.out.println("Not find!");
			}
		    

		
		return flag;
	}
	
	public static void SaveToFile()
	{
		FileWriter fstream;
		try
		{
			fstream = new FileWriter("topologies\\" + system.size + "\\"+ system.size + "_sim_"+ system.simIndex + ".txt");
			BufferedWriter out = new BufferedWriter(fstream);
			for(int i = 0 ; i < Landmark.Set.length ; i++)
			{
				out.write(String.valueOf(Landmark.Set[i].x));
				out.newLine();
				out.write(String.valueOf(Landmark.Set[i].y));
				out.newLine();
			}
			
			for(int i = 0 ; i < nodes.nodeSet.length ; i++)
			{
				out.write(String.valueOf(nodes.nodeSet[i].Coordination.x));
				out.newLine();
				out.write(String.valueOf(nodes.nodeSet[i].Coordination.y));
				out.newLine();
				System.out.println("Index = " + i + " " + nodes.nodeSet[i].Coordination.x + " " + nodes.nodeSet[i].Coordination.y);
			}
			
			out.close();
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	}
	
	//Based on the configured algorithm for name id assignment, this function resets the data structures of that algorithm
	private static void nameIDAssignmentReset()
	{
	     switch(system.nameIDAssignment)
	     {
		     case "LAND":
		    	 Algortihm1_RandomAssignment.reset();
		    	 break;
		     case "DPAD":
		     	 Algorithm6_dynamicPrefixAverageDistance.reset();
		     	 break;
		     case "MAM":
		         Algorithm2_MatrixAndMean.reset();
		         break;
		     case "MAME":
		    	 Algorithm3_MatrixAndMeanExtended.reset();
		    	 break;
		     case "LDHT":
		    	Algorithm4_RegionPrefix.reset();
		    	break;
		     case "Hirearchical":
		        Algorithm5_RegionDynamicPrefix.reset();
		        break;
		     case "LMDS":
		    	 Algorithm7_LMDS.reset();
		    	 break;
		     case "ELMDS":
		    	 Algorithm8_LMDSExtended.reset();
		    	 break;
		     case "DPLMDSE":
		    	 Algorithm9_DynamicPrefixLMDSHuffman.reset();
		    	 break;
		     case "DPLMDS":
		    	 Algorithm10_DynamicPrefixLMDS.reset();
		    	 break;
		     case "FPR":
		     	Algorithm11_FixedPrefixRandom.reset();
		     	break;
		     case "DPR":	
		    	 Algorithm12_DynamicPrefixRandom.reset();
		    	 break;
		     case "RSR":
		    	 Algorithm13_RegionSubRegion.reset();
		    	 break;
	     }
	}
	
	//Based on the algorithm that is configured for name id assignment, this function assigns name id
	private static String nameIDAssignment(node n)
	{
	     switch(system.nameIDAssignment)
	     {
		     case "LAND":
		    	 return Algortihm1_RandomAssignment.RandomNameIDAssignment();
		    	 
		     case "DPAD":
		     	 return Algorithm6_dynamicPrefixAverageDistance.Algorithm(n);
		   
		     case "MAM":
		         return Algorithm2_MatrixAndMean.Algorithm(n);
		         
		     case "MAME":
		    	 return Algorithm3_MatrixAndMeanExtended.Algorithm(n);

		     case "LDHT":
		    	return Algorithm4_RegionPrefix.Algorithm(n);
		  
		     case "Hirearchical":
		        return Algorithm5_RegionDynamicPrefix.Algorithm(n);

		     case "LMDS":
		    	 Algorithm7_LMDS.Algorithm();
		    	 return null;
		     case "ELMDS":
		    	 Algorithm8_LMDSExtended.Algorithm();
		    	 return null;
		     case "DPLMDSE":
		    	 Algorithm9_DynamicPrefixLMDSHuffman.Algorithm();
		    	 return null;
		     case "DPLMDS":
		    	 Algorithm10_DynamicPrefixLMDS.Algorithm();
		    	 return null;
		     case "FPR":
		     	return Algorithm11_FixedPrefixRandom.Algorithm(n);

		     case "DPR":	
		    	return Algorithm12_DynamicPrefixRandom.Algorithm(n);
		    	 
		     case "RSR":
		    	return Algorithm13_RegionSubRegion.Algorithm(n);
		    default: 
		    	return null;
		    	 
	     }
	}
	//Replicate based on the configured replication algorithm
	private static void replication()
	{
	    switch(system.replicationAlg)
	    {
	    	case "LP":
	    		if(system.replicationType.equals("public"))
	    			Rep_Alg01_LP.Algorithm();
	    		else
	    			Rep_Alg06_PrivateLP.Algorithm();
	    		break;
	    	case "randomized":
	    		if(system.replicationType.equals("public"))
	    			Rep_Alg02_Random.Algorithm();
	    		else
	    			Rep_Alg08_PrivateRandom.Algorithm();
	    		break;
	    	case "onneighbors":
	    		if(system.replicationType.equals("public"))
	    			Rep_Alg03_RepOnNeighbors.Algorithm();
	    		else
	    			Rep_Alg09_PrivateRepOnNeighbors.Algorithm();
	    		break;
	    	case "onpath":
	    		if(system.replicationType.equals("public"))
	    			Rep_Alg04_RepOnPath.Algorithm();
	    		else
	    			Rep_Alg10_PrivateRepOnPath.Algorithm();
	    		break;
	    	case "SLP":
	    		if(system.replicationType.equals("public"))
	    			Rep_Alg05_SubRegion.Algorithm();
	    		else
	    			Rep_Alg07_PrivateSubRegion.Algorithm();
	    		break;
	    	case "SSLP":
	    		if(system.replicationType.equals("public"))
	    			Rep_Alg11_SubProblemSubRegion.Algorithm(); 
	    		else
	    			Rep_Alg12_PrivateSubProblemSubRegion.Algorithm();
	    		break;
	    	case "LARAS":
	    		if(system.replicationType.equals("public"))
	    		   	  Rep_Alg13_AdaptiveSubProblemSubRegion.Algorithm();
	    		else
	    			Rep_Alg14_PrivateAdaptiveSubProblemSubRegion.Algorithm();
	    		break;
	    	case "onrequesters":
	    		Rep_Alg15_PrivateRepOnRequesters.Algorithm();
	    }


	}
}
