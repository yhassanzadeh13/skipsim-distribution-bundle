
import net.sf.javailp.Linear;
import net.sf.javailp.OptType;
import net.sf.javailp.Problem;
import net.sf.javailp.Result;
import net.sf.javailp.Solver;
import net.sf.javailp.SolverFactory;
import net.sf.javailp.SolverFactoryLpSolve;

import java.time.LocalTime;

import lpsolve.*;


public class Rep_Alg14_PrivateAdaptiveSubProblemSubRegion 
{
       
	 public static void Algorithm()
	 {
		 repTools.reset();
		 repTools.privateRepShareDefining();
		 repTools.adaptivePrivateSubproblemSizeDefining();
		 repTools.tablesInit();
		 repTools.replicaSetInit();
		 //repTools.replicaSetGenerator(repTools.PrivateOptimizer(repTools.realDistance, repTools.getProblemSize()), "Real", repTools.getProblemSize());
		 
		 
		 for(int i = 0 ; i < system.landmarks ; i++)
		 {	 
			 repTools.setMNR (repTools.getRepshare(i));
			 if(repTools.getMNR() == 0)
				 continue;
			 repTools.setAdaptiveSubProblemSize(i);
			 repTools.replicaSetGenerator2(repTools.PrivateOptimizer(repTools.nameidsDistance, repTools.getSubProblemSize()), "Local", repTools.getSubProblemSize(), i);
			 //validityTest();
			 //realWordTransform(i);
			 repTools.localReplicaSetInit();			 
		 }
		 
		 repTools.replicaAssignmentSetGenerator(repTools.getProblemSize());
		 //assignToOther();
		 double localDelay = repTools.privateAverageDelay(repTools.realWorldReplicaAssignment);
	     //int realDelay  = repTools.privateTotalDelay(repTools.realReplicaAssignment);
         //System.out.println("Local Delay " + localDelay);
         //System.out.println("Real Delay "  + realDelay);


		 
		 double ratio = (double)localDelay; ///realDelay;
		 System.out.println("Average Delay " + ratio + " Run " + system.simIndex);
		 repTools.setRatioDataSet(system.simIndex - 1, ratio);
		 
		 repEvaluation.privateReplicationLoadAnalysis(repTools.getProblemSize(), repTools.realWorldReplicaAssignment,repTools.getM());
		 if(system.simIndex == system.simRun)
		 {
			 repEvaluation.loadEvaluation();
			 repTools.evaluation(" Algorithm 14 PrivateAdaptiveSubProblemSubRegion "); 
		 }
     
         
	 }
}