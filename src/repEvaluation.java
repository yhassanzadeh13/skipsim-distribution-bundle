
public class repEvaluation 
{
	private static double[] loadDataSet  = new double[system.simRun];
	public static void privateReplicationLoadAnalysis(int problemSize, boolean[][] set, int M)
    {
   	 int[] replicaLoad = new int[problemSize];
   	 for(int i = 0 ; i < problemSize ; i++)
   		 replicaLoad[i] = 0;
   	 for(int i = 0 ; i < problemSize ; i++)
   		 for(int j = 0 ; j < problemSize ; j++)
   		 {
   			 if(set[i][j] && j % M == 0)
   			 {
   				 replicaLoad[i] = replicaLoad[i] + 1;
   			 }
   		 }
   	 double ave = 0;
   	 for(int i = 0; i < problemSize ; i++)
   	 {
   		 ave +=replicaLoad[i]; 
   		 //System.out.println(replicaLoad[i] + " ");
   	 }
   	 
   	ave  =  ave / system.MNR;
   	 
   	 double sd = 0;
   	 for(int i = 0; i < problemSize ; i++)
   	 {	
   		 if(replicaLoad[i] > 0)
   			 sd += Math.pow(replicaLoad[i] - ave, 2);
   			 
   	 }
   	 
   	 //System.out.println("Sum of SD " + sd + " " + ave);
   	 sd = sd / system.MNR;
   	 //System.out.println("ave of SD " + sd);
   	 sd = Math.sqrt(sd);
   	 
   	 
   	 loadDataSet[system.simIndex - 1] = sd;
   	 //System.out.println("SD Load on a replica " + (int) ave + " " + (int) sd);    	 
    }
	
	public static void publicReplicationLoadAnalysis(int problemSize, boolean[][] set)
    {
   	 int[] replicaLoad = new int[problemSize];
   	 for(int i = 0 ; i < problemSize ; i++)
   		 replicaLoad[i] = 0;
   	 for(int i = 0 ; i < problemSize ; i++)
   		 for(int j = 0 ; j < problemSize ; j++)
   		 {
   			 if(set[i][j])
   			 {
   				 replicaLoad[i] = replicaLoad[i] + 1;
   			 }
   		 }
   	 double ave = 0;
   	 for(int i = 0; i < problemSize ; i++)
   	 {
   		 ave +=replicaLoad[i]; 
   		 //System.out.println(replicaLoad[i] + " ");
   	 }
   	 
   	ave  =  ave / system.MNR;
   	 
   	 double sd = 0;
   	 for(int i = 0; i < problemSize ; i++)
   	 {	
   		 if(replicaLoad[i] > 0)
   			 sd += Math.pow(replicaLoad[i] - ave, 2);
   			 
   	 }
   	 
   	 //System.out.println("Sum of SD " + sd + " " + ave);
   	 sd = sd / system.MNR;
   	 //System.out.println("ave of SD " + sd);
   	 sd = Math.sqrt(sd);
   	 
   	 
   	 loadDataSet[system.simIndex - 1] = sd;
   	 //System.out.println("SD Load on a replica " + (int) ave + " " + (int) sd);    	 
    }
    
    public static void loadEvaluation()
    {
   	 
	   	 double ave  = 0;
	   	 double sd   = 0;
	   	 for(int i = 0; i < system.simRun ; i++)
	   	 {
	   		 ave +=loadDataSet[i];
	   		 //System.out.print(loadDataSet[i] + " ");
	   	 }
	   	 
	     //System.out.println("Sum of ave " + ave);
	   	 ave  = ave / system.simRun;
	   	 
	   	 for(int i = 0; i < system.simRun ; i++)
	   	 {
	   			 sd += Math.pow(loadDataSet[i] - ave, 2);
	   	 }
	   	 
	   	 sd = sd / system.simRun;
	   	 sd = Math.sqrt(sd);
	   	 
	   	 System.out.println("The average load on a replica is " + ave + " with the SD of " +  sd ); 
    }
}
