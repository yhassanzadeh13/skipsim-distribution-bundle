import java.util.Random;


public class Algorithm12_DynamicPrefixRandom 
{
  public static String[] nameSpace = new String[system.size];
  private static int nameSpaceIndex = 0;
  public static boolean nameIDGeneration = true;
  
  
      public static void reset()
      {
    	  nameSpace = new String[system.size];
    	  nameSpaceIndex = 0;
    	  nameIDGeneration = true;
      }
	  public static void nameIDGenerator(int n)
	  {
		        String B;
		    	for(int i = 0; i < Math.pow(2,n); i++)
		        {
		            B = "";
		            int temp = i;
		            for (int j = 0; j < n; j++)
		            {
		                if (temp%2 == 1)
		                    B = '1'+B;
		                else
		                    B = '0'+B;
		                    temp = temp/2;
		            }
		            //System.out.println(B);
		            addToNamespace(B);
		         }
		     
	  }
	  
	  public static void addToNamespace(String B)
	  {
		  if(nameSpaceIndex < system.size)
		  {
			  nameSpace[nameSpaceIndex] = B;
			  nameSpaceIndex++;
		  }
	  }
	  
	  public static String RandomNameIDAssignment(int closestLandmarkIndex)
	  {  
		  Random random = new Random();
		  int index = random.nextInt(system.size-1);
		  
		  int counter = 0;
		  while(nameSpace[index] == null && counter < 100)
		  {
		      index = random.nextInt(system.size-1);
		      counter ++;
		  }
		  
		  if(counter >= 100)
			  for(int i = 0 ; i < system.size ; i++)
			  {
				  if(nameSpace[i] != null)
				  {
					  index = i;
					  break;
				  }
				  
			  }
		  
		  String nameID = nameSpace[index];
		  nameSpace[index] = null;
          nameID = Landmark.dynamicPrefix[closestLandmarkIndex] + nameID; 
          
		return nameID;
	}
    
	 public static int ClosestLandmark(node n)
	 {
		 double min = Double.MAX_VALUE;
		 int index = 0;
		 for(int i = 0 ; i < system.landmarks ; i++)
		 {
			 if(n.Coordination.distance(Landmark.Set[i]) < min)
			 {
				min = n.Coordination.distance(Landmark.Set[i]);
				index = i;
			 }
		 }
		 
		 return index;
	 }
	  
	public static String Algorithm(node n)
	{
		  if(nameIDGeneration)
		  {
			  Landmark.upDateDynamicPrefix();
			  nameIDGenerator(system.landmarks);
			  nameIDGeneration = false;
		  }
		  
		  return RandomNameIDAssignment(ClosestLandmark(n));
	}
}