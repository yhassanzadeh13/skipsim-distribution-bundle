	import java.util.ArrayList;
import java.util.ArrayList;

public class Algorithm4_RegionPrefix
{

		 public static int [][] B = new int[system.landmarks][system.landmarks-system.prefix]; //binary matrix
		 public static double []   D = new double[system.landmarks - system.prefix]; //Distances to the landmarks
		 public static double []   M = new double[system.landmarks - system.prefix]; //Mean of distances to the landmarks
		 public static int nodeIndex = 0; //Number of nodes arriving to the system so far
		 public static String[] nameSpace = new String[system.size];
		 private static int nameSpaceIndex = 0;
		 public static boolean nameIDGeneration = true;
		 public static String nameID = new String();
		 public static ArrayList<String> extendSet = new ArrayList<String>(); 
          
		 
		  public static void reset()
		  {
				 B = new int[system.landmarks][system.landmarks-system.prefix]; //binary matrix
				 D = new double[system.landmarks - system.prefix]; //Distances to the landmarks
				 M = new double[system.landmarks - system.prefix]; //Mean of distances to the landmarks
				 nodeIndex = 0; //Number of nodes arriving to the system so far
				 nameSpace = new String[system.size];
				 nameSpaceIndex = 0;
				 nameIDGeneration = true;
				 nameID = new String();
				 ArrayList<String> extendSet = new ArrayList<String>(); 			  
		  }
		  
		  public static void nameIDGenerator(int n)
		  {
			        String B;
			    	for(int i = 0; i < Math.pow(2,n); i++)
			        {
			            B = "";
			            int temp = i;
			            for (int j = 0; j < n; j++)
			            {
			                if (temp%2 == 1)
			                    B = '1'+B;
			                else
			                    B = '0'+B;
			                    temp = temp/2;
			            }
			            
			            addToNamespace(B);
			         }
			     
		  }
		  
			 public static void InitializingB()
			 {

					 for(int i = 0 ; i < system.landmarks-system.prefix ; i++)
					 {
						 for(int j = 0 ; j < (system.landmarks - i) ; j++)
						 {
							 B[j][i] =  1;
						 }
						 for(int j = system.landmarks-i ; j < system.landmarks ; j++)
						 {
							 B[j][i] = 0;
						 }
					 }
		 
			 }
			 
			 public static void PrintB()
			 {
				 System.out.println("B is : ");
				 for(int i = 0 ; i < system.landmarks ; i++)
				 {
					 for(int j = 0 ; j < system.landmarks - system.prefix ; j++)
					 {
						 System.out.print(B[i][j]);
						 
					 }
					 
					 System.out.println(" ");
				 }
			 }
		  
		  public static void addToNamespace(String B)
		  {
			  nameSpace[nameSpaceIndex] = B;
			  nameSpaceIndex++;
		  }
		  
		  public static void InitializingD()
		  {
			  for(int i = 0 ; i < system.landmarks-system.prefix ; i++)
				  D[i] = 0;
		  }
			 public static void GeneratingD(node n)
			 {
				 for(int i = 0 ; i < system.landmarks-system.prefix ; i++)
					 D[i] = 0;
				 
				 
	             
				 for(int i = 1 ; i < system.landmarks-system.prefix ; i++)
				 {
					 for(int j = 0 ; j < system.landmarks ; j++)
					 {
						 D[i] += n.Coordination.distance(Landmark.Set[i]) * B[j][i];	 
					 }
					 
					 
				 }
				 
				
				 nodeIndex ++;
				// UpdatingM();
			 }
		 
		 public static int ClosestLandmark(node n)
		 {
			 double min = Double.MAX_VALUE;
			 int index = 0;
			 for(int i = 0 ; i < system.landmarks ; i++)
			 {
				 if(n.Coordination.distance(Landmark.Set[i]) < min)
				 {
					min = n.Coordination.distance(Landmark.Set[i]);
					index = i;
				 }
			 }
			 
			 return index;
		 }
		 
		 public static void UpdatingM()
		 {
			
			 for(int i = 0 ; i < system.landmarks-system.prefix ; i++)
			 {
				 M[i] = M[i] * (nodeIndex - 1) + D[i];
			     M[i] = M[i] / nodeIndex;
			     System.out.print(D[i] + " ");
			 }   
			 System.out.println(" ");
			 System.out.println("The M updated as: ");
			 for(int i = 0 ; i < system.landmarks - system.prefix ; i++)
				 System.out.print(M[i] + "  ");
			 System.out.println(" ");
		 }
		 
		 public static void InitilizingM()
		 {
			 for(int i = 0 ; i < M.length ; i++)
				 M[i] = 0;
		 }
		 
		 public static String NameIDGenerating(int closestLandmarkIndex)
		 {
			 nameID = new String();
			 
			 nameID = nameID + Landmark.prefix[closestLandmarkIndex];		 
			 
			 for(int i = 0 ; i < D.length - Landmark.prefix[closestLandmarkIndex].length() ; i++)
			 {
				 if(D[i] > (M[i]))
					 nameID = nameID + "0";
				 else
					 nameID = nameID + "1";
			 }
			 
			 UpdatingM();
			 
			 if(nameSpace[Integer.parseInt(nameID , 2)] == null)
			 { 
				 nameID = extend(nameID);
				 extendSet.add(nameID);
				 System.out.println("The Extended nameID is:");
				 return nameID;
		     }
			 
			 else
			 {
			     nameID = nameSpace[Integer.parseInt(nameID, 2)];
			     nameSpace[Integer.parseInt(nameID, 2)] = null;
			     return nameID;
			 }
			 
          
			 
			 
		 }
		 
		 
		 public static String Algorithm(node n)
		 {
		  if(nameIDGeneration)
		  {
				  nameIDGenerator(system.landmarks);
				  nameIDGeneration = false;
				  InitializingB();
				  InitilizingM();
				  PrintB();
		  }

			 InitializingD();
			 
			 GeneratingD(n);
			 
			 return NameIDGenerating(ClosestLandmark(n));
		 }
		 
		 public static String extend(String nameID)
		 {
	       // System.out.println("extend receives " + nameID);
			String[] Split = nameID.split("_");
			//System.out.println("Split is "  + Split);
	        nameID = new String();
	        
	        for(int i = 0 ; i < Split.length ; i++)
	        {
	        	if(!extendSet.contains(Split[i] + "0"))
	        		return Split[i] + "0";
	        	else if (!extendSet.contains(Split[i] + "1"))
	        	{
					return Split[i] + "1";
				}
	        	
	        	else
	        	{
	        		
	        		nameID = nameID + Split[i] + "0_";
	        		nameID = nameID + Split[i] + "1";
	        		if(i != Split.length - 1)
	        			nameID = nameID + "_";
	        	}
	        	
	        	
	        }
	        
	        //System.out.println("The extend function wants to recurse the " + nameID);
	        return extend(nameID);
		 }
		 
		 
		 
		 
		 

	


}
