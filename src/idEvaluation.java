import java.io.*;
import java.util.ArrayList;

public class idEvaluation
{	  
	  public static double[][] Results  = new double[system.simRun][system.graphNameIDSize];
	  public static void maltabRepAutoMeanSDEvaluation()
	  {
		  //system.nameIDsize = 12;
		  double[] SD = new double[system.graphNameIDSize];
		  double[] Mean = new double[system.graphNameIDSize];
		  try
		   {
				    // Create file 
					   
					
					double[] DistanceFromEachOther = new double[system.graphNameIDSize]; // sum of distances of nodes that have i (0<= i < system.nameIDsize) from eachother 
					int[] NumberOfNodes = new int[system.graphNameIDSize]; // number of the nodes that have i (0<= i < system.nameIDsize) bits in their common prefix

					
				    if(system.maltabRepAutoMeanSDEvaluationInit)
			        {

                        for(int i = 0 ; i < system.simRun ; i++)
                        	for(int j = 0 ; j < system.graphNameIDSize ; j++)
                        		Results[i][j] = 0;
                        
				        system.maltabRepAutoMeanSDEvaluationInit = false;
			        }
				    
			        for(int i = 0 ; i < system.graphNameIDSize ; i++)
			        {
			        	DistanceFromEachOther[i] = 0;
			        	NumberOfNodes[i] = 0;
			        	SD[i] = 0;
			        	Mean[i] = 0;
			        }
			        


				        int c = 0;
					    for(int i = 0 ; i < system.size ; i++)
					    {
					    	//System.out.println("First for");
					    	for(int j = i+1 ; j < system.size ; j++)
					    	{
					    		c = nodes.commonBits(i, j) ;
					    		//System.out.println("commonbits = " + c);
//					    		if(c < 2*system.nameIDsize)
					    		{
					    			//System.out.println("C<size");
					    			NumberOfNodes[c] = NumberOfNodes[c] + 1;
					    			DistanceFromEachOther[c] = DistanceFromEachOther[c] + nodes.nodeSet[i].Coordination.distance(nodes.nodeSet[j].Coordination);
					    			//if(c == 4) System.out.println("Distance = " + DistanceFromEachOther[c]);
					    		}
//					    		else
//					    		{
//					    			//System.out.println("C>size");
//					    			NumberOfNodes[system.nameIDsize-1] = NumberOfNodes[system.nameIDsize-1] + 1;
//					    			DistanceFromEachOther[system.nameIDsize-1] = DistanceFromEachOther[system.nameIDsize-1] + nodes.nodeSet[i].Coordination.distance(nodes.nodeSet[j].Coordination); 
//					    			
//					    		}
					    							    		
						    }
					    }
					    
					    for(int i = 0 ; i < system.graphNameIDSize ; i++)
					    {

					    	//SumMeansOfAll[i] = SumMeansOfAll[i] + (DistanceFromEachOther[i] / NumberOfNodes[i]);
					    	if(NumberOfNodes[i] == 0) 
					    		Results[system.simIndex-1][i] = 0;
					    	else
					    	    Results[system.simIndex-1][i] = (DistanceFromEachOther[i] / NumberOfNodes[i]);
					    	//if(i == 4) System.out.println("Run " + system.simIndex + "  mean " + Results[system.simIndex-1][i] + " " + NumberOfNodes[i] + " " +  DistanceFromEachOther[i]);
					    }
					    
					    if(system.simIndex == system.simRun) 
					    {
					    	//system.nameIDsize = 12;
					    	System.out.println("Matlab format Mean Evaluation  started!");    
						    FileWriter fstream = new FileWriter("evaluationSDMeanResult.txt");
						    BufferedWriter out = new BufferedWriter(fstream);
					    	for(int i = 0 ; i < system.graphNameIDSize ; i++)
					    	{
							    for(int j = 0 ; j < system.simRun; j++)
								    {

	                                  //Mean[i] = SumMeansOfAll[i] / system.simRun;
						    		  Mean[i] = Mean[i] + Results[j][i];   		 		
								    }
							    
							    Mean[i] = Mean[i] / system.simRun;
					    	}
						    
							    for(int i = 0 ; i < system.graphNameIDSize ; i++)
							    {
							    	for(int j = 0 ; j < system.simRun ; j++)
							    	{

							    			SD[i] = SD[i] + Math.pow((Results[j][i] - Mean[i]) , 2);						    							    		
								    }
							    }
						    
							    for(int i = 0 ; i < system.graphNameIDSize ; i++)
							    {
						    		out.write(String.valueOf(i));
					    			out.write(" ");
					    			    		
					    		
					    		  if(NumberOfNodes[i] != 0)
					    			  out.write(String.valueOf((int)(Mean[i])));
					    		  else
					    			  out.write(String.valueOf(0));
					    		   
					    		    out.write(" ");
					    		    
					    		  if(SD[i] != 0)
					    			  out.write(String.valueOf((int)(Math.sqrt(SD[i]/system.simRun)/2)));
					    		  else 
					    			  out.write(String.valueOf(0));
					    			out.newLine();
					    			   		 		
							    }
						    
						    
						    
						    //Close the output stream
						    out.close();
						    System.out.println("Evaluation is done!");
					    }
					    

					   
		   }

	       
		         
		   catch (Exception e)
		   {
			   
			   //Catch exception if any
		      e.printStackTrace();
		   }
	   
	 
	  
	  }
}
