import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Random;

import javax.naming.TimeLimitExceededException;
import javax.xml.ws.FaultAction;


public class RecoveryEvaluation 
{
   public static int networkLifeTime = 0;
   public static int networkLifeTimeAverage = 0;
   final static int  recoveryRun     = 100;
   public static int recoveryIndex  = 0;
   
   
   public static boolean init = true;
   public static int runTime = 1000;
   public static int currentTime = 0;
   public static int[] failTrans = new int[runTime];
   public static int failTransCount = 0;
   
   public static int[][] failTransCountDataBase = new int[system.simRun][runTime];
   
   public static void networkLifeTime()
   {
     for( ; recoveryIndex < recoveryRun ; recoveryIndex++)
     {
       nodes.energyReset();
	   while(true)
       {
     	  Random random = new Random();
     	  String nameID = nodes.nodeSet[random.nextInt(system.size-1)].nameID;
     	  int i = random.nextInt(system.size-1);
     	  int result = SkipGraphOperations.SearchByNameID(nameID, i);
     	  if(result < 0)
     		  break;
     	  String nameResult = nodes.nodeSet[result].nameID;
     	  String searchStatus = new String();
     	  if(nameResult == nameID)
     	  {
     		  searchStatus = "Correct Transaction";
     		  networkLifeTime++;
     	  }
     	  else
     		  searchStatus = "Failed   Transaction";
     	  System.out.println("Serach for " + nameID + " is " + result + " with name ID  " + nameResult + "  Status:  " +  searchStatus + "Min Battery : " + 
     		                   nodes.minBatteryStatus() + "  Battery Average  " + nodes.networkBatteryAverage() );
     	  if(nameResult!= nameID)
     		  break;
       } 
	   
	   System.out.println(networkLifeTime);
	   networkLifeTimeAverage += networkLifeTime; 
	   networkLifeTime = 0;
     }
    
     networkLifeTimeAverage = networkLifeTimeAverage / recoveryRun;
     System.out.println("\n\n\nThe average of network life time is: " + networkLifeTimeAverage);
   }
   
   
   
   public static void TimeMonitoring()
   {
     if(init)
     {
    	 for(int i = 0 ; i < runTime ; i++)
    		 failTrans[i] = 0;
    	 init = false;
     }
	 
     
     //nodes.energyReset();
	 for( ; currentTime < runTime ; currentTime++)
     {
		 
		 
		  
		  System.out.println("Time " + currentTime + " Failure nodes "  +  failTransCount +
				 " Min Battery " + nodes.minBatteryStatus() + "  Battery Average  " + nodes.networkBatteryAverage() );
		  Random random = new Random();
     	  String nameID = nodes.nodeSet[random.nextInt(system.size-1)].nameID;
     	  int i = random.nextInt(system.size-1);
     	  int result = SkipGraphOperations.SearchByNameID(nameID, i);
     	  if(result < 0)
     	  { 
     		  failTransCount++;
     		  failTrans[currentTime] = failTransCount;
     		  continue;
     	  }
     	  
     	  String nameResult = nodes.nodeSet[result].nameID;
     	  if(nameResult != nameID)
     	  {
     		  failTransCount++;
     		  failTrans[currentTime] = failTransCount;
     		  continue;     		 
     	  }
     	  
     	  
     	 failTrans[currentTime] = failTransCount;
     	    
     }
     
	 System.out.println("Time = [");
	 for(int i = 0 ; i < runTime ; i+=10)
		 System.out.print(i + " ");
	 System.out.println("]");
	 
	 System.out.println("Failure = [");
	 for(int i = 0 ; i < runTime ; i+=10)
		 System.out.print(failTrans[i] + " ");
	 System.out.println("]");
	 
   }
   
   public static void multiTimeMonitoring()
   {
     if(init)
     {
    	 for(int i = 0 ; i < runTime ; i++)
    	 {
    		 failTrans[i] = 0;
    		 for(int j = 0 ; j < system.simRun ; j++)
    			 failTransCountDataBase[j][i] = 0;
    	 }
    	 init = false;
     }
	 
     failTransCount = 0;
     //nodes.energyReset();
	 for(currentTime = 0 ; currentTime < runTime ; currentTime++)
     {
		 
		 
		  
		  System.out.println("Time " + currentTime + " Failure nodes "  +  failTransCount +
				 " Min Battery " + nodes.minBatteryStatus() + "  Battery Average  " + nodes.networkBatteryAverage() );
		  Random random = new Random();
     	  String nameID = nodes.nodeSet[random.nextInt(system.size-1)].nameID;
     	  int i = random.nextInt(system.size-1);
     	  int result = SkipGraphOperations.SearchByNameID(nameID, i);
     	  if(result < 0)
     	  { 
     		  failTransCount++;
     		  failTransCountDataBase[system.simIndex-1][currentTime] = nodes.networkBatteryAverage();//failTransCount;
     		  continue;
     	  }
     	  
     	  String nameResult = nodes.nodeSet[result].nameID;
     	  if(nameResult != nameID)
     	  {
     		  failTransCount++;
     		  failTransCountDataBase[system.simIndex-1][currentTime] = nodes.networkBatteryAverage();//failTransCount;
     		  continue;     		 
     	  }
     	  
     	  
     	 failTransCountDataBase[system.simIndex-1][currentTime] = nodes.networkBatteryAverage();//failTransCount;
     	    
     }
	 
	    if(system.simIndex == system.simRun) 
	    {
	    	try
	    	{
		    	System.out.println("Matlab format Recovery Evaluation  started!");    
			    FileWriter fstream = new FileWriter("RecoveryEvaluationResult.txt");
			    BufferedWriter out = new BufferedWriter(fstream);
		    	int[] Mean = new int[runTime];
		    	int[] SD   = new int[runTime]; 
			    for(int j = 0 ; j < system.simRun; j++)
			    {
	    		  Mean[j] = 0;  
	    		  SD[j] = 0;
			    }
		    	for(int i = 0 ; i <runTime  ; i+=10)
		    	{
				    for(int j = 0 ; j < system.simRun; j++)
					    {
			    		  Mean[i] = Mean[i] + failTransCountDataBase[j][i];   		 		
					    }
				    
				    Mean[i] = Mean[i] / system.simRun;
		    	}
			    
				for(int i = 0 ; i < runTime ; i+=10)
				{
					for(int j = 0 ; j < system.simRun ; j++)
					{
				
							SD[i] = (int) (SD[i] + Math.pow((failTransCountDataBase[j][i] - Mean[i]) , 2));						    							    		
				    }
				}
			    
			    for(int i = 0 ; i < runTime ; i+=10)
			    {
		    		out.write(String.valueOf(i));
	    			out.write(" ");
	    			    		
	    			out.write(String.valueOf((int)(Mean[i])));
	    			 
	    		   
	    		    out.write(" ");
	    		    
	    		  if(SD[i] != 0)
	    			  out.write(String.valueOf((int)(Math.sqrt(SD[i]/system.simRun)/2)));
	    		  else 
	    			  out.write(String.valueOf(0));
	    			out.newLine();
	    			   		 		
			    }
			    
			    
			    
			    //Close the output stream
			    out.close();
			    System.out.println("Evaluation is done!");
		    }
	 	   catch (Exception e)
	 	   {
	 		   e.printStackTrace();
	 	   }
	    	
	    }
	    
	 
   }  
   
}
