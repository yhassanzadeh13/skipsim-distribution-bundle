
import net.sf.javailp.Linear;
import net.sf.javailp.OptType;
import net.sf.javailp.Problem;
import net.sf.javailp.Result;
import net.sf.javailp.Solver;
import net.sf.javailp.SolverFactory;
import net.sf.javailp.SolverFactoryLpSolve;

import java.time.LocalTime;

import lpsolve.*;


public class Rep_Alg07_PrivateSubRegion 
{

    
	 public static void Algorithm()
	 {
		 repTools.reset();
		 repTools.privateRepShareDefining();
		 repTools.tablesInit();
		 repTools.replicaSetInit();
		 //repTools.replicaSetGenerator(repTools.PrivateOptimizer(repTools.realDistance, repTools.getProblemSize()), "Real", repTools.getProblemSize());

		 System.out.println(LocalTime.now());
		 for(int i = 0 ; i < system.landmarks ; i++)
		 {	 
			 repTools.setMNR (repTools.getRepshare(i));
			 if(repTools.getMNR() == 0)
				 continue;
			 repTools.replicaSetGenerator2(repTools.PrivateOptimizer(repTools.nameidsDistance, repTools.getNameSpace()), "Local", repTools.getNameSpace(), i);
			 //validityTest();
			 //realWordTransform(i);
			 repTools.localReplicaSetInit();
		 }
		 
		 repTools.replicaAssignmentSetGenerator(repTools.getProblemSize());
		 //assignToOther();
		 double localDelay = repTools.privateAverageDelay(repTools.realWorldReplicaAssignment);
	     //int realDelay  = repTools.privateTotalDelay(repTools.realReplicaAssignment);
         //System.out.println("Average Latency " + localDelay);
         //System.out.println("Local Delay " + realDelay);

//	     if(localDelay == 0)
//	     {
//	    	 if(system.simIndex == 1)
//	    		 ratioDataSet[system.simIndex - 1] = 1;
//	    	 else
//	    	     ratioDataSet[system.simIndex - 1] = ratioDataSet[system.simIndex - 2];
//	     }
//	     else
	     {
			// double ratio = (double)localDelay/realDelay;
			 System.out.println("Average Latency " + localDelay + " Run " + system.simIndex);
			 repTools.setRatioDataSet(system.simIndex - 1, localDelay);
	     }

		 System.out.println(LocalTime.now());
		 
		 if(system.simIndex == system.simRun)
		 {
			 repTools.evaluation(" Algorithm 07 PrivateSubRegion "); 
		 }

     
         
	 }
}
