
public class system 
{
         public static int size = 512;
         public static int landmarks = 9;
         public static int index = -1;
         public static int UniformLandmarkThereshould = 50;
         public static int UniformSumOfLandmarkThereshould = 1000;
         public static int LandmarkBasedLandmarkThereshould = 5;
         public static int LandmarkBasedSumOfLandmarkThereshould = 200;
         public static int prefix = 3;
         public static int nameIDsize = 9;
         public static int domainSize = 3000;
         public static int simRun = 100;
         public static int simIndex = 0;
         public static int graphNameIDSize = 20;
         public static boolean maltabRepAutoMeanSDEvaluationInit = true;
         
         
         //public static double totalRTTDelay = 0;
         public static int criticalBatteryLevel = 20;
         public static boolean backup = false;
         
         public static int algorithmNumber = 1;
         
         public static int MNR = 25;
         public static int NOR = 153;
         
         //Algorithms parameters for node generation, name id and numerical id distribution and replication
         public static String nodeGeneration = "landmark";
         public static String numericalIDAssignment = "randomized";
         public static String nameIDAssignment = "DPAD";
         public static String replicationType = "public";
         public static String replicationAlg  = "None";
         //Evaluation algorithms
         public static int searchByNumericalID = 0;
         public static int searchByNameID      = 0; 
         public static boolean idEvaluation  = false;
         public static boolean repEvaluation = false;  
         public static boolean loadEvaluation = false;
         
         //Whether name id is assigned statically or dynamically
         //This parameter is adjust by system
         public static boolean dynamicNameID = true;
         
         
         public static void reset()
         {
        	 index = -1;
         }
}
