
public class nodes 
{
  public static int nodeIndex = 0; //number of the nodes
  public static node[] nodeSet = new node[system.size];
  private static int totalTime = 0;

  
  public static void timeIncrease(int amount)
  {
//	  if(totalTime < amount)
//		  totalTime = amount;
	  totalTime += amount;

  }
  
  public static int getTotalTime()
  {
	  return totalTime;
  }
  
  public static void addTime(int destination, int source)
  {
	  if(destination != -1 && source != -1)
	       totalTime += nodes.nodeSet[source].Coordination.distance(nodes.nodeSet[destination].Coordination);
  }
  
  public static void resetTotalTime()
  {
	  System.out.println("TotalTime Reset from " + totalTime);
	  totalTime = 0;
	  //totalNum  = 0;
  }
  
  public static void reset()
  {
	  nodeSet = new node[system.size];
	  nodeIndex = 0;
	  totalTime = 0;
  }
  
  public static void addNodeToSet(node n)
  {
	 if(nodeIndex < system.size)
	 {
		
		n.address = nodeIndex;
		nodeSet[nodeIndex] = n;
		//System.out.println(nodeIndex + " " + nodeSet[nodeIndex].Coordination.x + " " + nodeSet[nodeIndex].Coordination.y);
		if(nodeIndex > 0 && system.dynamicNameID)
			SkipGraphOperations.Insert(nodeIndex);
		nodeIndex++;

		
	 }
  }
  
  
  public static int commonBits(int i, int j)
  {
	  String s1 = nodeSet[i].nameID;
	  String s2 = nodeSet[j].nameID;

	  int k = 0;
	  while(s1.charAt(k) == s2.charAt(k))
	  {
		 k++;
	     if(k >= s1.length() || k >= s2.length())// || k >= system.nameIDsize )
	    	 break;
	  }
		  
	  return k;
  }
  
  public static int minNameIDSize(int i, int j)
  {
	  String s1 = nodeSet[i].nameID;
	  String s2 = nodeSet[j].nameID;

      if(s1.length() < s2.length())
    	  return s1.length();
      
      else
    	  return s2.length();
  }
  
  public static int minBatteryStatus()
  {
	  int min = Integer.MAX_VALUE;
	  int index = 0;
	  for(int i = 0 ; i < system.size ; i++)
	  {
		  if(nodeSet[i].batteryLevel< min)
		  {
			  min = (int) nodeSet[i].batteryLevel;
			  index = i;
		  }
		  
	  }
	  
	  return (int) nodeSet[index].batteryLevel;
  }
  
  public static int networkBatteryAverage()
  {
	  double sum = 0;
	  for(int i = 0 ; i < system.size ; i++)
		  sum += nodeSet[i].batteryLevel;
	  sum = sum / system.size;
	  
	  return (int) sum;
	  
  }
  
  public static void energyReset()
  {
	  for(int i = 0 ; i < system.size ; i++)
	  {
		  nodes.nodeSet[i].energyReset();
	  }
  }
  
  public static boolean networkShutdownCheck()
  {
	  for(int i = 0 ; i < system.size ; i++)
	  {
		  if(!nodes.nodeSet[i].isDeactive())
			  return false;
			  
	  }
	  return true;
  }
  
	 public static int ClosestLandmark(int nodeIndex)
	 {
		 node n = nodes.nodeSet[nodeIndex];
		 double min = Double.MAX_VALUE;
		 int index = 0;
		 for(int i = 0 ; i < system.landmarks ; i++)
		 {
			 if(n.Coordination.distance(Landmark.Set[i]) < min)
			 {
				min = n.Coordination.distance(Landmark.Set[i]);
				index = i;
			 }
		 }
		 
		 return index;
	 }
	 

  
  
}
